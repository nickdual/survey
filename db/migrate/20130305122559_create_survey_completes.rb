class CreateSurveyCompletes < ActiveRecord::Migration
  def change
    create_table :survey_completes do |t|
      t.integer :user_id
      t.integer :survey_id
      t.integer :survey_status_id
      t.text :notes
      t.boolean :done_by_eo, default: false

      t.timestamps
    end
  end
end
