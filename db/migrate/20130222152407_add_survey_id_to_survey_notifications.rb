class AddSurveyIdToSurveyNotifications < ActiveRecord::Migration
  def change
    add_column :survey_notifications, :survey_id, :integer
  end
end
