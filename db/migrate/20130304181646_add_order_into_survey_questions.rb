class AddOrderIntoSurveyQuestions < ActiveRecord::Migration
  def up
    add_column :survey_questions, :order, :integer, default: 0
  end

  def down
  end
end
