class AddDetailToUser < ActiveRecord::Migration
  def up
    add_column :users, :gender, :string, :limit => 8
    add_column :users, :age, :integer, :limit => 2
    add_column :users, :zip, :string, :limit => 64
  end
end
