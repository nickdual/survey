class AddIndexsIntoTables < ActiveRecord::Migration
  def up
    add_index :surveys, :user_id
    add_index :survey_completes, :user_id
    add_index :survey_completes, :survey_id
    add_index :survey_notifications, :user_id
    add_index :survey_notifications, :survey_id
    add_index :survey_possible_answers, :survey_question_id
    add_index :survey_questions, :survey_id
    add_index :survey_questions, :survey_question_type_id
    add_index :survey_user_answers, :user_id
    add_index :survey_user_answers, :survey_possible_answer_id
    add_index :survey_user_permissions, :user_id
    add_index :survey_user_permissions, :survey_id
  end

  def down
  end
end
