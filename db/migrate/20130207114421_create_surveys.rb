class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.string :title, null: false
      t.text :audience_description, null: false
      t.text :description, null: false
      t.string :document
      t.boolean :active, default: false
      t.integer :user_id, null: false

      t.timestamps
    end
  end
end
