class CreateMarketingMaterials < ActiveRecord::Migration
  def change
    create_table :marketing_materials do |t|
      t.string :title
      t.date :published_date
      t.string :image_url
      t.string :destination_url
      t.boolean :active, defalut: true

      t.timestamps
    end
  end
end
