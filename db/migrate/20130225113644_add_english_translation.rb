class AddEnglishTranslation < ActiveRecord::Migration
  def change
    add_column :survey_questions, :english_translation, :string
    add_column :survey_possible_answers, :english_translation, :string
  end
end
