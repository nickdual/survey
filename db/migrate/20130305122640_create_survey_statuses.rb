class CreateSurveyStatuses < ActiveRecord::Migration
  def change
    create_table :survey_statuses do |t|
      t.string :name
    end
  end
end
