class CreateSurveyUserPermissions < ActiveRecord::Migration
  def change
    create_table :survey_user_permissions do |t|
      t.integer :user_id
      t.integer :survey_id
      t.boolean :edit, default: false
      t.boolean :invite, default: false

      t.timestamps
    end
  end
end
