class CreateSurveyNotifications < ActiveRecord::Migration
  def change
    create_table :survey_notifications do |t|
      t.integer :user_id
      t.string :text

      t.timestamps
    end
  end
end
