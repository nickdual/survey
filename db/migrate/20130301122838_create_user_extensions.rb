class CreateUserExtensions < ActiveRecord::Migration
  def change
    create_table :user_extensions do |t|
      t.integer :user_id
      t.string :company
      t.string :company_role

      t.timestamps
    end
  end
end
