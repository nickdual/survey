class AddEnglishTranslationIntoSurveyNotifications < ActiveRecord::Migration
  def up
    add_column :survey_notifications, :english_translation, :string
  end

  def down
  end
end
