class CreateSurveyUserAnswers < ActiveRecord::Migration
  def change
    create_table :survey_user_answers do |t|
      t.integer :user_id, null: false
      t.integer :survey_possible_answer_id, null: false
      t.string :text

      t.timestamps
    end
  end
end
