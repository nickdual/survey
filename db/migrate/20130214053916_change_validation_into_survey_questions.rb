class ChangeValidationIntoSurveyQuestions < ActiveRecord::Migration
  def up
    change_column :survey_questions, :validation, :string, defalut: ''
    change_column :survey_possible_answers, :validation, :string, defalut: ''
  end

  def down
    change_column :survey_questions, :validation, :string
    change_column :survey_possible_answers, :validation, :string
  end
end
