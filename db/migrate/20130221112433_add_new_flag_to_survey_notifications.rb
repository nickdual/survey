class AddNewFlagToSurveyNotifications < ActiveRecord::Migration
  def change
    add_column :survey_notifications, :new_flag, :boolean, default: true
  end
end
