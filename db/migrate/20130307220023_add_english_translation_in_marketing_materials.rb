class AddEnglishTranslationInMarketingMaterials < ActiveRecord::Migration
  def change
    add_column :marketing_materials, :english_translation, :string
  end
end
