class CreateSurveyGraphTypes < ActiveRecord::Migration
  def change
    create_table :survey_graph_types do |t|
      t.string :name, null: false
      t.string :description, null: false

      t.timestamps
    end
  end
end
