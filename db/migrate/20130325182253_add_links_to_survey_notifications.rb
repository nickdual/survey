class AddLinksToSurveyNotifications < ActiveRecord::Migration
  def up
  	add_column :survey_notifications, :links, :string    
  end
 
  def down
    remove_column :survey_notifications, :links
  end
end
