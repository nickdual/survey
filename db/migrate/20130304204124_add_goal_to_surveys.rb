class AddGoalToSurveys < ActiveRecord::Migration
  def up
    add_column :surveys, :goal, :integer
  end

  def down
  end
end
