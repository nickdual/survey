class CreateSurveyQuestions < ActiveRecord::Migration
  def change
    create_table :survey_questions do |t|
      t.integer :survey_id, null: false
      t.integer :survey_question_type_id, null: false
      t.integer :survey_graph_type_id, null: false
      t.string  :text, null: false
      t.boolean :validation, default: false
      t.integer :other_answer_id

      t.timestamps
    end
  end
end
