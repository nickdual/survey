class CreateSurveyPossibleAnswers < ActiveRecord::Migration
  def change
    create_table :survey_possible_answers do |t|
      t.integer :survey_question_id, null: false
      t.string :text, null: false
      t.boolean :validation, default: false
      t.integer :next_question_id
      t.string :special_action

      t.timestamps
    end
  end
end
