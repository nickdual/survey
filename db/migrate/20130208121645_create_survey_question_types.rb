class CreateSurveyQuestionTypes < ActiveRecord::Migration
  def change
    create_table :survey_question_types do |t|
      t.string :name, null: false
      t.string :description, null: false

      t.timestamps
    end
  end
end
