
SurveyQuestion.seed do |f|
  f.id = 1
  f.survey_id = 1
  f.survey_question_type_id = 11
  f.survey_graph_type_id = 1
  f.text = "Welcome"
  f.validation = "f"
  f.english_translation = "Welcome"
  f.order = 1
end

SurveyQuestion.seed do |f|
  f.id = 2
  f.survey_id = 1
  f.survey_question_type_id = 12
  f.survey_graph_type_id = 1
  f.text = "Qualified"
  f.validation = "f"
  f.english_translation = "Qualified"
  f.order = 2
end
SurveyQuestion.seed do |f|
  f.id = 3
  f.survey_id = 1
  f.survey_question_type_id = 13
  f.survey_graph_type_id = 1
  f.text = "Declined"
  f.validation = "f"
  f.english_translation = "Declined"
  f.order = 3
end
SurveyQuestion.seed do |f|
  f.id = 4
  f.survey_id = 1
  f.survey_question_type_id = 6
  f.survey_graph_type_id = 1
  f.text = "First Question"
  f.validation = "f"
  f.english_translation = "First Question"
  f.order = 0
end
SurveyQuestion.seed do |f|
  f.id = 5
  f.survey_id = 1
  f.survey_question_type_id = 1
  f.survey_graph_type_id = 1
  f.text = "Multi Choice"
  f.validation = "f"
  f.english_translation = "Multi Choice"
  f.order = 3
end
SurveyQuestion.seed do |f|
  f.id = 6
  f.survey_id = 1
  f.survey_question_type_id = 15
  f.survey_graph_type_id = 1
  f.text = "Hello question"
  f.validation = "f"
  f.english_translation = "Hello question"
  f.order = 1
end

SurveyQuestion.seed do |f|
  f.id = 7
  f.survey_id = 2
  f.survey_question_type_id = 11
  f.survey_graph_type_id = 1
  f.text = "Welcome"
  f.validation = "f"
  f.english_translation = "Welcome"
  f.order = 1
end

SurveyQuestion.seed do |f|
  f.id = 8
  f.survey_id = 2
  f.survey_question_type_id = 12
  f.survey_graph_type_id = 1
  f.text = "Qualified"
  f.validation = "f"
  f.english_translation = "Qualified"
  f.order = 2
end
SurveyQuestion.seed do |f|
  f.id = 9
  f.survey_id = 2
  f.survey_question_type_id = 13
  f.survey_graph_type_id = 1
  f.text = "Declined"
  f.validation = "f"
  f.english_translation = "Declined"
  f.order = 3
end
SurveyQuestion.seed do |f|
  f.id = 10
  f.survey_id = 2
  f.survey_question_type_id = 6
  f.survey_graph_type_id = 1
  f.text = "First Question"
  f.validation = "f"
  f.english_translation = "First Question"
  f.order = 0
end
SurveyQuestion.seed do |f|
  f.id = 11
  f.survey_id = 2
  f.survey_question_type_id = 1
  f.survey_graph_type_id = 1
  f.text = "Multi Choice"
  f.validation = "f"
  f.english_translation = "Multi Choice"
  f.order = 3
end
SurveyQuestion.seed do |f|
  f.id = 12
  f.survey_id = 2
  f.survey_question_type_id = 15
  f.survey_graph_type_id = 1
  f.text = "Text freeform"
  f.validation = "f"
  f.english_translation = "Hello question"
  f.order = 1
end

SurveyQuestion.seed do |f|
  f.id = 13
  f.survey_id = 2
  f.survey_question_type_id = 2
  f.survey_graph_type_id = 1
  f.text = "multi choice"
  f.validation = "f"
  f.english_translation = "multi choice"
  f.order =1
  end
SurveyQuestion.seed do |f|
  f.id = 14
  f.survey_id = 3
  f.survey_question_type_id = 15
  f.survey_graph_type_id = 1
  f.text = "mutiple text"
  f.validation = "f"
  f.english_translation = "multi text"
  f.order =1
end

SurveyQuestion.seed do |f|
  f.id = 15
  f.survey_id = 3
  f.survey_question_type_id = 14
  f.survey_graph_type_id = 1
  f.text = "Image chooser(Select Many)"
  f.validation = "f"
  f.english_translation = "Image chooser(Select Many)"
  f.order =1
end
SurveyQuestion.seed do |f|
  f.id = 16
  f.survey_id = 3
  f.survey_question_type_id = 2
  f.survey_graph_type_id = 1
  f.text = "Multiple-choice(Select Multiple)"
  f.validation = "f"
  f.english_translation = "Multiple-choice(Select Multiple)"
  f.order =1
end

SurveyQuestion.seed do |f|
  f.id = 17
  f.survey_id = 3
  f.survey_question_type_id = 11
  f.survey_graph_type_id = 1
  f.text = "Welcome"
  f.validation = "f"
  f.english_translation = "welcome"
  f.order =1
end

SurveyQuestion.seed do |f|
  f.id = 18
  f.survey_id = 3
  f.survey_question_type_id = 12
  f.survey_graph_type_id = 1
  f.text = "Qualified"
  f.validation = "f"
  f.english_translation = "Qualified"
  f.order =1
end
SurveyQuestion.seed do |f|
  f.id = 19
  f.survey_id = 3
  f.survey_question_type_id = 13
  f.survey_graph_type_id = 1
  f.text = "Decilined"
  f.validation = "f"
  f.english_translation = "Decilined)"
  f.order =1
end

