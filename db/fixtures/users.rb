
User.seed do |s|
  s.id = 1
  s.email = "abc@gmail.com"
  s.encrypted_password = "$2a$10$IviKcFWCIHK8INb13PYnQ.PkDv8nzhRlBM6iGrJGfRmZkyETal1lG"
  s.first_name = "Hello"
  s.last_name = "World"
  s.city = "City"
  s.phone_number = "0123456789"
  s.admin = "TRUE"
  s.role = "admin"
  s.gender = 'male'
  s.age = '20'
end
User.seed do |s|
  s.id = 2
  s.email = "example@gmail.com"
  s.encrypted_password = "$2a$10$IviKcFWCIHK8INb13PYnQ.PkDv8nzhRlBM6iGrJGfRmZkyETal1lG"
  s.first_name = "Hello"
  s.last_name = "World"
  s.city = "City"
  s.phone_number = "0123456789"
  s.admin = "TRUE"
  s.role = "admin"
  s.gender = 'female'
  s.age = '34'
  end
User.seed do |s|
  s.id = 3
  s.email = "email@gmail.com"
  s.encrypted_password = "$2a$10$IviKcFWCIHK8INb13PYnQ.PkDv8nzhRlBM6iGrJGfRmZkyETal1lG"
  s.first_name = "Hello"
  s.last_name = "World"
  s.city = "City"
  s.phone_number = "0123456789"
  #s.admin = "TRUE"
  s.role = "admin"
  s.gender = 'male'
  s.age = '60'
end