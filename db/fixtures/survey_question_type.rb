
SurveyQuestionType.seed do |f|
  f.id = 1
  f.name = "Multiple-choice(Select One)"
  f.description = "Multiple-choice(Select One)"
  end
SurveyQuestionType.seed do |f|
  f.id = 2
  f.name = "Multiple-choice(Select Multiple)"
  f.description = "Multiple-choice(Select Multiple)"
  end
SurveyQuestionType.seed do |f|
  f.id = 3
  f.name = "Rank order"
  f.description = "Rank order"
  end
SurveyQuestionType.seed do |f|
  f.id = 4
  f.name = "Constant sum"
  f.description = "Constant sum"
  end
SurveyQuestionType.seed do |f|
  f.id = 5
  f.name = "Drop-down menu"
  f.description = "Drop-down menu"
  end
SurveyQuestionType.seed do |f|
  f.id = 6
  f.name = "Number Freeform Input"
  f.description = "Number Freeform Input"
  end
SurveyQuestionType.seed do |f|
  f.id = 7
  f.name = "Comment Box"
  f.description = "Comment Box"
  end
SurveyQuestionType.seed do |f|
  f.id = 8
  f.name = "Matrix multi-point scales select one"
  f.description = "Matrix multi-point scales select one"
  end
SurveyQuestionType.seed do |f|
  f.id = 9
  f.name = "Matrix table multi-select"
  f.description = "Matrix table multi-select"
  end
SurveyQuestionType.seed do |f|
  f.id = 10
  f.name = "Image chooser(Select One)"
  f.description = "Image chooser(Select One)"
  end
SurveyQuestionType.seed do |f|
  f.id = 11
  f.name = "Welcome"
  f.description = "Welcome"
  end
SurveyQuestionType.seed do |f|
  f.id = 12
  f.name = "Qualified"
  f.description = "Qualified"
  end
SurveyQuestionType.seed do |f|
  f.id = 13
  f.name = "Declined"
  f.description = "Declined"
  end
SurveyQuestionType.seed do |f|
  f.id = 14
  f.name = "Image chooser(Select Many)"
  f.description = "Image chooser(Select Many)"
  end
SurveyQuestionType.seed do |f|
  f.id = 15
  f.name = "Text Freeform Input"
  f.description = "Text Freeform Input"
  end
