
SurveyPossibleAnswer.seed do |f|
  f.id = 1
  f.survey_question_id = 4
  f.text = '123'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 1

end

SurveyPossibleAnswer.seed do |f|
  f.id = 2
  f.survey_question_id = 4
  f.text = '222'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 1

end

SurveyPossibleAnswer.seed do |f|
  f.id = 3
  f.survey_question_id = 5
  f.text = 'A'
  f.validation = 'f'
  f.english_translation = 'a'
  f.next_question_id = 2
end
SurveyPossibleAnswer.seed do |f|
  f.id = 4
  f.survey_question_id = 5
  f.text = 'B'
  f.validation = 'f'
  f.english_translation = 'b'
  f.next_question_id = 3
end
SurveyPossibleAnswer.seed do |f|
  f.id = 5
  f.survey_question_id = 5
  f.text = 'C'
  f.validation = 'f'
  f.english_translation = 'c'
  f.next_question_id = 4
end
SurveyPossibleAnswer.seed do |f|
  f.id = 6
  f.survey_question_id = 5
  f.text = 'D'
  f.validation = 'f'
  f.english_translation = 'd'
  f.next_question_id = 5
end
SurveyPossibleAnswer.seed do |f|
  f.id = 7
  f.survey_question_id = 6
  f.text = 'D'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 5
end
SurveyPossibleAnswer.seed do |f|
  f.id = 8
  f.survey_question_id = 6
  f.text = 'A'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 3
end
SurveyPossibleAnswer.seed do |f|
  f.id = 9
  f.survey_question_id = 6
  f.text = 'C'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 4
end
SurveyPossibleAnswer.seed do |f|
  f.id = 10
  f.survey_question_id = 6
  f.text = 'B'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 1
end


SurveyPossibleAnswer.seed do |f|
  f.id = 11
  f.survey_question_id = 12
  f.text = 'D'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 5
end
SurveyPossibleAnswer.seed do |f|
  f.id = 12
  f.survey_question_id = 12
  f.text = 'A'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 3
end
SurveyPossibleAnswer.seed do |f|
  f.id = 13
  f.survey_question_id =12
  f.text = 'C'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 4
end
SurveyPossibleAnswer.seed do |f|
  f.id = 14
  f.survey_question_id = 12
  f.text = 'B'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 1
end

SurveyPossibleAnswer.seed do |f|
  f.id = 15
  f.survey_question_id = 11
  f.text = 'D'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 5
end
SurveyPossibleAnswer.seed do |f|
  f.id = 16
  f.survey_question_id = 11
  f.text = 'A'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 3
end


SurveyPossibleAnswer.seed do |f|
  f.id = 24
  f.survey_question_id = 14
  f.text = 'D'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 17
end
SurveyPossibleAnswer.seed do |f|
  f.id = 25
  f.survey_question_id = 14
  f.text = 'A'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 17
end

SurveyPossibleAnswer.seed do |f|
  f.id = 26
  f.survey_question_id = 14
  f.text = 'C'
  f.validation = '=='
  f.english_translation = ''
  f.next_question_id = 18
end
SurveyPossibleAnswer.seed do |f|
  f.id = 27
  f.survey_question_id = 15
  f.text = 'A'
  f.validation = 'false'
  f.english_translation = 'A'
  f.next_question_id = 16
end

SurveyPossibleAnswer.seed do |f|
  f.id = 28
  f.survey_question_id = 15
  f.text = 'C'
  f.validation = 'false'
  f.english_translation = 'C'
  f.next_question_id = 17
end
SurveyPossibleAnswer.seed do |f|
  f.id = 29
  f.survey_question_id = 15
  f.text = 'B'
  f.validation = 'false'
  f.english_translation = 'B'
  f.next_question_id = 18
end

SurveyPossibleAnswer.seed do |f|
  f.id = 30
  f.survey_question_id = 15
  f.text = 'D'
  f.validation = 'false'
  f.english_translation = 'D'
  f.next_question_id = 19
end
SurveyPossibleAnswer.seed do |f|
  f.id = 31
  f.survey_question_id = 16
  f.text = 'A'
  f.validation = 'f'
  f.english_translation = 'A'
  f.next_question_id = 15
end

SurveyPossibleAnswer.seed do |f|
  f.id = 32
  f.survey_question_id = 16
  f.text = 'C'
  f.validation = 'f'
  f.english_translation = 'C'
  f.next_question_id = 16
end
SurveyPossibleAnswer.seed do |f|
  f.id = 33
  f.survey_question_id = 16
  f.text = 'B'
  f.validation = 'f'
  f.english_translation = 'B'
  f.next_question_id = 18
end

SurveyPossibleAnswer.seed do |f|
  f.id = 34
  f.survey_question_id = 16
  f.text = 'D'
  f.validation = 'f'
  f.english_translation = 'D'
  f.next_question_id = 15
end

