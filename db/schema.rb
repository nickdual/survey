# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130403032703) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "answers", :force => true do |t|
    t.string   "letter"
    t.string   "text"
    t.string   "response"
    t.boolean  "correct"
    t.integer  "question_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "answers", ["question_id", "letter"], :name => "index_answers_on_question_id_and_letter"
  add_index "answers", ["question_id"], :name => "index_answers_on_question_id"

  create_table "appointments", :force => true do |t|
    t.datetime "scheduled_for"
    t.integer  "job_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "appointments", ["job_id"], :name => "index_appointments_on_job_id"
  add_index "appointments", ["scheduled_for"], :name => "index_appointments_on_scheduled_for"

  create_table "bryant", :id => false, :force => true do |t|
    t.integer  "user_id"
    t.integer  "count",   :limit => 8
    t.datetime "min"
    t.datetime "max"
  end

  create_table "campaigns", :force => true do |t|
    t.text     "text"
    t.string   "keyword"
    t.string   "processing_type"
    t.integer  "company_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "message_type_prefix"
    t.boolean  "disabled"
    t.integer  "row_order"
    t.integer  "submenu_id"
    t.string   "description"
    t.boolean  "archived",            :default => false
  end

  add_index "campaigns", ["archived"], :name => "index_campaigns_on_archived"
  add_index "campaigns", ["keyword"], :name => "index_campaigns_on_keyword"
  add_index "campaigns", ["submenu_id"], :name => "index_campaigns_on_submenu_id"

  create_table "checkins", :force => true do |t|
    t.integer  "status"
    t.string   "address"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "gmaps"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "checkins", ["latitude"], :name => "index_checkins_on_latitude"
  add_index "checkins", ["longitude"], :name => "index_checkins_on_longitude"
  add_index "checkins", ["status"], :name => "index_checkins_on_status"
  add_index "checkins", ["user_id"], :name => "index_checkins_on_user_id"

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "components", :force => true do |t|
    t.text     "text"
    t.string   "keyword"
    t.boolean  "capture"
    t.integer  "next_component_id"
    t.integer  "campaign_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "components", ["campaign_id"], :name => "index_components_on_campaign_id"
  add_index "components", ["keyword"], :name => "index_components_on_keyword"

  create_table "consulates", :id => false, :force => true do |t|
    t.integer  "id",                   :null => false
    t.string   "name"
    t.string   "state"
    t.string   "keyword"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "estimated_traffic"
    t.string   "phone_number"
    t.string   "monitor_phone_number"
    t.string   "region"
  end

  create_table "event_participations", :force => true do |t|
    t.integer  "event_id"
    t.integer  "user_id"
    t.boolean  "admin"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "event_participations", ["user_id"], :name => "index_event_participations_on_user_id"

  create_table "event_types", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "events", :force => true do |t|
    t.datetime "event_time"
    t.integer  "event_type_id"
    t.string   "title"
    t.string   "description"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "organizing_user_id"
    t.boolean  "messaged"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "events", ["event_time"], :name => "index_events_on_event_time"

  create_table "features", :force => true do |t|
    t.integer  "consulate_id"
    t.integer  "campaign_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "features", ["campaign_id"], :name => "index_features_on_campaign_id"
  add_index "features", ["consulate_id", "campaign_id"], :name => "index_features_on_consulate_id_and_campaign_id"
  add_index "features", ["consulate_id"], :name => "index_features_on_consulate_id"

  create_table "industries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "interests", :force => true do |t|
    t.integer  "user_id"
    t.integer  "topic_id"
    t.string   "priority"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "interests", ["topic_id", "user_id"], :name => "index_interests_on_topic_id_and_user_id", :unique => true
  add_index "interests", ["user_id", "topic_id"], :name => "index_interests_on_user_id_and_topic_id", :unique => true

  create_table "jobs", :force => true do |t|
    t.integer  "status"
    t.datetime "status_updated_at"
    t.string   "description"
    t.string   "address"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "gmaps"
    t.integer  "estimated_hours"
    t.integer  "estimated_price"
    t.integer  "user_customer_id"
    t.integer  "user_worker_id"
    t.integer  "industry_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "checkin_id"
    t.boolean  "own_supplies"
    t.boolean  "cleaning_level"
    t.integer  "rooms"
    t.integer  "bathrooms"
  end

  add_index "jobs", ["checkin_id"], :name => "index_jobs_on_checkin_id"
  add_index "jobs", ["industry_id"], :name => "index_jobs_on_industry_id"
  add_index "jobs", ["user_customer_id"], :name => "index_jobs_on_user_ordering_id"
  add_index "jobs", ["user_worker_id"], :name => "index_jobs_on_user_assigned_id"

  create_table "marketing_materials", :force => true do |t|
    t.string   "title"
    t.date     "published_date"
    t.string   "image_url"
    t.string   "destination_url"
    t.boolean  "active"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "english_translation"
  end

  create_table "menus", :force => true do |t|
    t.text     "text"
    t.integer  "consulate_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "menus", ["consulate_id"], :name => "index_menus_on_consulate_id"

  create_table "message_types", :force => true do |t|
    t.string   "name"
    t.string   "setting_key"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "campaign_id"
    t.integer  "component_id"
  end

  create_table "offers", :force => true do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "checkin_id"
    t.integer  "status"
  end

  add_index "offers", ["checkin_id"], :name => "index_offers_on_checkin_id"
  add_index "offers", ["job_id"], :name => "index_offers_on_job_id"
  add_index "offers", ["status"], :name => "index_offers_on_status"
  add_index "offers", ["user_id"], :name => "index_offers_on_user_id"

  create_table "participations", :force => true do |t|
    t.integer  "user_id"
    t.integer  "campaign_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "participations", ["user_id", "campaign_id"], :name => "index_participations_on_user_id_and_campaign_id"

  create_table "questions", :force => true do |t|
    t.string   "text"
    t.integer  "point_value"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "question_type"
  end

  create_table "ratings", :force => true do |t|
    t.integer  "fun"
    t.integer  "fast"
    t.integer  "skilled"
    t.integer  "user_customer_id"
    t.integer  "user_worker_id"
    t.integer  "job_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "ratings", ["job_id"], :name => "index_ratings_on_job_id"
  add_index "ratings", ["user_customer_id"], :name => "index_ratings_on_user_rating_id"
  add_index "ratings", ["user_worker_id"], :name => "index_ratings_on_user_rated_id"

  create_table "settings", :force => true do |t|
    t.string   "key"
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "settings", ["key"], :name => "index_settings_on_key"

  create_table "skills", :force => true do |t|
    t.integer  "rating"
    t.integer  "wage"
    t.integer  "user_id"
    t.integer  "industry_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "skills", ["industry_id"], :name => "index_skills_on_industry_id"
  add_index "skills", ["user_id"], :name => "index_skills_on_user_id"

  create_table "spell_checkers", :force => true do |t|
    t.string   "typo"
    t.string   "correction"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "submenus", :force => true do |t|
    t.string   "keyword"
    t.integer  "row_order"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "survey_completes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "survey_id"
    t.integer  "survey_status_id"
    t.text     "notes"
    t.boolean  "done_by_eo",       :default => false
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  add_index "survey_completes", ["survey_id"], :name => "index_survey_completes_on_survey_id"
  add_index "survey_completes", ["user_id"], :name => "index_survey_completes_on_user_id"

  create_table "survey_graph_types", :force => true do |t|
    t.string   "name",        :null => false
    t.string   "description", :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "survey_notifications", :force => true do |t|
    t.integer  "user_id"
    t.string   "text"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.boolean  "new_flag",            :default => true
    t.integer  "survey_id"
    t.string   "english_translation"
    t.string   "links"
  end

  add_index "survey_notifications", ["survey_id"], :name => "index_survey_notifications_on_survey_id"
  add_index "survey_notifications", ["user_id"], :name => "index_survey_notifications_on_user_id"

  create_table "survey_possible_answers", :force => true do |t|
    t.integer  "survey_question_id",                     :null => false
    t.string   "text",                                   :null => false
    t.string   "validation",          :default => false
    t.integer  "next_question_id"
    t.string   "special_action"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "english_translation"
  end

  add_index "survey_possible_answers", ["survey_question_id"], :name => "index_survey_possible_answers_on_survey_question_id"

  create_table "survey_question_types", :force => true do |t|
    t.string   "name",        :null => false
    t.string   "description", :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "survey_questions", :force => true do |t|
    t.integer  "survey_id",                                  :null => false
    t.integer  "survey_question_type_id",                    :null => false
    t.integer  "survey_graph_type_id",                       :null => false
    t.string   "text",                                       :null => false
    t.string   "validation",              :default => false
    t.integer  "other_answer_id"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "english_translation"
    t.integer  "order",                   :default => 0
  end

  add_index "survey_questions", ["survey_id"], :name => "index_survey_questions_on_survey_id"
  add_index "survey_questions", ["survey_question_type_id"], :name => "index_survey_questions_on_survey_question_type_id"

  create_table "survey_statuses", :force => true do |t|
    t.string "name"
  end

  create_table "survey_user_answers", :force => true do |t|
    t.integer  "user_id",                   :null => false
    t.integer  "survey_possible_answer_id", :null => false
    t.string   "text"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "survey_user_answers", ["survey_possible_answer_id"], :name => "index_survey_user_answers_on_survey_possible_answer_id"
  add_index "survey_user_answers", ["user_id"], :name => "index_survey_user_answers_on_user_id"

  create_table "survey_user_permissions", :force => true do |t|
    t.integer  "user_id"
    t.integer  "survey_id"
    t.boolean  "edit",       :default => false
    t.boolean  "invite",     :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "survey_user_permissions", ["survey_id"], :name => "index_survey_user_permissions_on_survey_id"
  add_index "survey_user_permissions", ["user_id"], :name => "index_survey_user_permissions_on_user_id"

  create_table "surveys", :force => true do |t|
    t.string   "title",                                   :null => false
    t.text     "audience_description",                    :null => false
    t.text     "description",                             :null => false
    t.string   "document"
    t.boolean  "active",               :default => false
    t.integer  "user_id",                                 :null => false
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.integer  "goal"
  end

  add_index "surveys", ["user_id"], :name => "index_surveys_on_user_id"

  create_table "tag_tickets", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "Ticket_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "tag_tickets", ["tag_id"], :name => "index_tag_tickets_on_tag_id"

  create_table "tag_users", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "tag_users", ["tag_id"], :name => "index_tag_users_on_tag_id"

  create_table "tags", :force => true do |t|
    t.string   "name"
    t.string   "keyword"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "text_responses", :force => true do |t|
    t.integer  "campaign_id"
    t.integer  "component_id"
    t.string   "answer"
    t.integer  "target_component_id"
    t.string   "action"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "texts", :force => true do |t|
    t.string   "direction"
    t.text     "message_body"
    t.integer  "message_type_id"
    t.string   "returned_status"
    t.boolean  "correct_guess"
    t.integer  "question_id"
    t.integer  "user_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "letter"
  end

  add_index "texts", ["direction", "created_at"], :name => "index_texts_on_direction_and_created_at"
  add_index "texts", ["message_body"], :name => "index_texts_on_message_body"
  add_index "texts", ["question_id"], :name => "index_texts_on_question_id"
  add_index "texts", ["user_id", "message_type_id"], :name => "index_texts_on_user_id_and_message_type_id"
  add_index "texts", ["user_id", "question_id", "letter"], :name => "index_texts_on_user_id_and_question_id_and_letter"
  add_index "texts", ["user_id"], :name => "index_texts_on_user_id"

  create_table "tickets", :force => true do |t|
    t.integer  "status"
    t.integer  "user_customer_id"
    t.integer  "user_agent_id"
    t.datetime "closed_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "tickets", ["status"], :name => "index_tickets_on_status"
  add_index "tickets", ["user_agent_id"], :name => "index_tickets_on_user_agent_id"
  add_index "tickets", ["user_customer_id"], :name => "index_tickets_on_user_customer_id"

  create_table "topics", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "topics", ["name"], :name => "index_topics_on_name"

  create_table "transformations", :force => true do |t|
    t.string   "typo"
    t.string   "correction"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "transformations", ["typo"], :name => "index_transformations_on_typo"

  create_table "user_extensions", :force => true do |t|
    t.integer  "user_id"
    t.string   "company"
    t.string   "company_role"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "",    :null => false
    t.string   "encrypted_password",                    :default => "",    :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "company"
    t.string   "company_role"
    t.string   "city"
    t.string   "phone_number"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
    t.boolean  "admin",                                 :default => false
    t.string   "role",                   :limit => 128
    t.string   "gender",                 :limit => 8
    t.integer  "age",                    :limit => 2
    t.string   "zip",                    :limit => 64
  end

  add_index "users", ["created_at"], :name => "index_users_on_created_at"
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["phone_number"], :name => "index_users_on_phone_number"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_warehouses", :id => false, :force => true do |t|
    t.boolean "subscribed"
    t.string  "role"
    t.string  "gender"
    t.integer "points"
    t.string  "first_name"
    t.string  "last_name"
    t.string  "consulate"
    t.boolean "gmaps"
    t.integer "total_texts",   :limit => 8
    t.integer "inbound_texts", :limit => 8
    t.text    "join_month"
  end

  create_table "workers", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "role"
    t.boolean  "do_not_contact"
    t.datetime "do_not_contact_date"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "workers", ["last_name"], :name => "index_workers_on_last_name"
  add_index "workers", ["phone_number"], :name => "index_workers_on_phone_number"

  create_table "zipcodes", :force => true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
