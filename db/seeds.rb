# Question types
SurveyQuestionType.create(name: 'Multiple-choice select one', description: 'Multi-choice select one')
SurveyQuestionType.create(name: 'Multiple-choice select multiple', description: 'Multiple-choice select multiple')
SurveyQuestionType.create(name: 'Rank order', description: 'Rank order')
SurveyQuestionType.create(name: 'Constant sum', description: 'Constant sum')
SurveyQuestionType.create(name: 'Drop-down menu', description: 'Drop-down menu')
SurveyQuestionType.create(name: 'Numeric Freeform Input', description: 'Numeric Freeform Input')
SurveyQuestionType.create(name: 'Text Freeform Input', description: 'Text Freeform Input')
SurveyQuestionType.create(name: 'Comment Box', description: 'Comment Box')
SurveyQuestionType.create(name: 'Matrix multi-point scales select one', description: 'Matrix multi-point scales select one')
SurveyQuestionType.create(name: 'Matrix table multi-select', description: 'Matrix table multi-select')
SurveyQuestionType.create(name: 'Image chooser(select one)', description: 'Image chooser')
SurveyQuestionType.create(name: 'Welcome', description: 'Welcome')
SurveyQuestionType.create(name: 'Qualified', description: 'Qualified')
SurveyQuestionType.create(name: 'Decliend', description: 'Decliend')

# Graph types
SurveyGraphType.create(name: 'Basic Pie Chart', description: 'Basic Pie Chart')
SurveyGraphType.create(name: 'Basic Column', description: 'Basic Column')
SurveyGraphType.create(name: 'Basic bar display averages ordered', description: 'Basic bar display averages ordered')
SurveyGraphType.create(name: 'Column for distribution + average', description: 'Column for distribution + average')
SurveyGraphType.create(name: 'Word cloud', description: 'Word cloud')

