module SurveysHelper
	def shuffled_colors
		colors = ['green', 'yellow', 'red', 'blue', 'orange', 'purple', 'pink', 'navy']
		colors.shuffle
	end
end
