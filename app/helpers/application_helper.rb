module ApplicationHelper
  def time_2_datetime_str(time)
    time.strftime("%A, %B %d, %Y, %l:%M %p %Z")
  end

  def time_2_date_str(time)
    time.strftime("%B %d, %Y, %l:%M %p")
  end

end
