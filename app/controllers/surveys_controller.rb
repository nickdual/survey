require 'json'

class SurveysController < ApplicationController
  before_filter :authenticate_user!, except: [:question_lang, :question_info, :question]
  before_filter :load_survey, only: [:show, :destroy, :edit, :results, :question, :invite, :share_list, :question_lang, :question_info]
  before_filter :load_user_by_survey_token, only: [:question_lang, :question_info, :question]
  autocomplete :state, :name
  
   def results
    create_custom_filters(params)

    @information = {
        :gender => { :male =>0, :female => 0},
        :age => {
            :group1 => {:from=> '18', :to => '24', :value => 0},
            :group2 => {:from=> '25', :to => '34', :value => 0},
            :group3 => {:from=> '35', :to => '44', :value=> 0},
            :group4 => {:from=> '45', :to => '54', :value => 0},
            :group5 => {:from=> '55', :to => '64', :value => 0},
            :group6 => {:from=> '65+', :to => '', :value => 0}
        },
        :total => 0
    }

    @user_ids.each do |user|
      user = User.find(user)
      @information[:total] += 1
      case user.gender
        when 'male'
          @information[:gender][:male] += 1
        when 'female'
          @information[:gender][:female] += 1
      end
      [18..24, 25..34, 35..44, 45..54, 55..64, 65..1.0/0].each_with_index do |range, index|
        @information[:age][:"group#{index+1}"][:value] += 1 if user.age.present? && (range.include? user.age)
      end
    end
    @information[:total] == 0 ? @information[:divide] = 1 : @information[:divide] = @information[:total]
  end



  def index
    authorize! :read, Survey
    @surveys = Survey.accessible_by(current_ability)
  end


  def show
    authorize! :read, Survey
    respond_to do |format|
      format.html
      format.xls
    end
  end

  def new
    @survey = Survey.new
  end

  def create
    authorize! :manage, Survey
    @survey = current_user.surveys.build(params[:survey])
puts @users.class()

    if @survey.save
      # make 4 default questions
      @survey.survey_questions.create( survey_question_type_id: 11, survey_graph_type_id: 1, text: 'Welcome', english_translation: 'Welcome',order: -5 )
      @survey.survey_questions.create( survey_question_type_id: 12, survey_graph_type_id: 1, text: 'Qualified', english_translation: 'Qualified', order:-3)
      @survey.survey_questions.create( survey_question_type_id: 13, survey_graph_type_id: 1, text: 'Declined', english_translation: 'Declined', order:-1 )
      @survey.survey_questions.create( survey_question_type_id: 1, survey_graph_type_id: 1, text: 'First Question', english_translation: 'First Question', order: 1 )

      UserMailer.survey_creation_email(current_user, @survey).deliver
      redirect_to surveys_path, notice: I18n.t("surveys.msg_create_success")
    else
      render action: "new"
    end
  end

  def edit
    authorize! :manage, Survey
  end

  def update
    authorize! :manage, Survey

    if @survey.update_attributes(params[:survey])
      redirect_to surveys_path, notice: I18n.t("surveys.msg_update_success")
    else
      render action: "edit"
    end
  end

  def destroy
    authorize! :manage, Survey
  end


  def question_lang
      load_user_by_survey_token
      puts "FNG 1"
    if @user.nil?
      puts "FNG 1 +i"
      #redirect_to survey_question_path(@survey)
      return
    end
      puts "FNG 2"

    if params[:locale].present?
      session[:locale] = params[:locale]
       if current_user==@user
        puts "Test 1"
        redirect_to survey_question_path(@survey)
       else
        redirect_to survey_question_info_path(@survey, survey_token: params[:survey_token], user_id: params[:user_id], eo: params[:eo], back_url: params[:back_url])
        puts "Test 2"
      end
    else
      render "surveys/question_lang", layout: 'layouts/survey_question'
      puts "Test 3"
    end
  end

  def question_info
      puts "Test 3.14 (pie!)"
    if @user.nil?
      render "surveys/questions/question_finish", layout: 'layouts/survey_question'
      puts "Test 4"
      return
    end
    if request.method == 'POST'
        puts "Test 4.8"

      if @user.update_attributes(params[:user])
        puts "Test 5"

        redirect_to survey_question_path(@survey, survey_token: params[:survey_token], user_id: params[:user_id], eo: params[:eo], back_url: params[:back_url])
        puts "Test 5.4"
      else
        puts "Test 6"
        render "surveys/question_info", layout: 'layouts/survey_question'
        puts "Test 6.4"
      end
    else
      puts "Test 7"

      render "surveys/question_info", layout: 'layouts/survey_question'
      puts "Test 7.4"
    end
  end

  def question
      puts "Test A.1"

    if @user.nil?
        puts " Test  ha?"
      flash.now[:notice] = I18n.t("surveys.msg_survey_permission")
      render "surveys/questions/question_finish", layout: 'layouts/survey_question'
      return
    end
      puts "Test A.2"

    if params[:question_id].blank?
      @question = @survey.survey_questions.order("id asc").first
      @question.step_number = 1
      render_question()
      return
    end
      puts "Test A.3"

    prev_question = SurveyQuestion.find(params[:question_id]).convert_to_child_class
    prev_question.step_number = params[:step_number]
      puts "Test A.5"

    # qualified/declined
    if [12, 13].include?(prev_question.survey_question_type_id)
      SurveyComplete.create(user_id: @user.id, survey_id: @survey.id, done_by_eo: params[:eo] == 'true')
      puts "Test A.7"

      if current_user
        redirect_to surveys_path
      else
        redirect_to params[:back_url].empty? ? root_path : params[:back_url]
      end
      puts "Test A.8"

      return
    end

    # validation check
    unless prev_question.validate?(params)
      @question = prev_question
      render_question()
      return
    end

    # submit question
    @question = prev_question.submit(@user, params, true)

    # next question checking
    if @question.present?
      @question.step_number = params[:step_number].to_i + 1

      render_question()
    else
      SurveyComplete.create(user_id: @user.id, survey_id: @survey.id, done_by_eo: params[:eo] == 'true')
      flash.now[:notice] = I18n.t("surveys.msg_no_next_question")
      render "surveys/questions/question_finish", layout: 'layouts/survey_question'
    end

  end

  def share
    @surveys = current_user.surveys + current_user.invitable_shared_surveys
  end

  def share_list
    render partial: 'share_survey'
  end

  def invite
    new_user_flag = false
    user = User.find_by_email(params[:invite_email])

    if user.nil?
      user = User.create!(email: params[:invite_email], password: User.reset_password_token, role: "client")
      new_user_flag = true
    elsif user.id == @survey.user_id
      render json: {success: false, message: I18n.t("surveys.msg_access_duplication")}
      return
    end

    if user.valid?
      if SurveyUserPermission.where(user_id: user.id, survey_id: @survey.id).first.nil?
        SurveyUserPermission.create(user_id: user.id, survey_id: @survey.id, invite: true)
        SurveyNotification.create(user_id: user.id, text: "#{current_user.to_s} le ha enviado una invitacion para que usted vea los resultados de la nueva encuesta.", english_translation: "#{current_user.to_s} has invited you to see the results for a new survey.", survey_id: @survey.id)
        UserMailer.invite_survey_to_user(current_user, user, params[:invite_message], @survey, new_user_flag).deliver
        render json: {success: true}
      else
        render json: {success: false, message: I18n.t("surveys.msg_access_duplication")}
      end
    else
      render json: {success: false, message: I18n.t("surveys.msg_invitation_failed")}
    end
  end


  def load_survey
    @survey = params[:id].nil? ? Survey.find(params[:survey_id]) : Survey.find(params[:id])
  end

  def render_question(message = nil)
    flash.now[:alert] = message if message
puts " Test B "+@question.to_s + "   B"
    respond_to do |format|
      format.html do
        render :question, layout: 'layouts/survey_question'
      end
    end
  end

  def load_user_by_survey_token

    if params[:survey_token].present?
      @user = User.find_by_id(params[:user_id])
      unless @user.nil?
        @user = nil if params[:survey_token] != @survey.generate_token(@user)
      end
    else
        @user=current_user
    end
if @user.nil?

puts "so confused"
puts "Test"
puts  params[:survey_token]
puts   User.find_by_id(params[:user_id])
puts params[:survey_token].present?
end


    if current_user.nil? and @user.nil?
        flash.now[:notice] = I18n.t("surveys.msg_survey_permission")
        render "surveys/questions/question_finish", layout: 'layouts/survey_question'
        puts "UnAuthorised acceess"
         return
    end
    end

  def create_custom_filters(params)
    user_ids = {}; answer_ids = {}
    @survey_questions = @survey.survey_questions.where("survey_question_type_id NOT IN (11, 12, 13)").includes(:survey_possible_answers, :survey_user_answers )
    filter = JSON.parse(params[:filterItems]) if params[:filterItems]
    @survey_questions.each do |question|
      question.survey_user_answers.includes(:user).each do |answer|
        next if answer.user.blank?
        expression = true
        if params.present? && params[:filterItems]
          filter.each do |key,value|
            case key
              when 'fromdate'
                expression &= answer.created_at >= value
              when 'todate'
                expression &= answer.created_at <= value
              when 'min_age_greator'
                expression &= answer.user.age > value if answer.user.age
              when 'min_age_less'
                expression &= answer.user.age < value if answer.user.age
              when 'gender'
                expression &= answer.user.gender.downcase == value.downcase if answer.user.gender
            end
          end
        end
        if expression
          user_ids[answer.user.id] = true
          answer_ids[answer.id] = true
        end
      end
    end
    @answer_ids = [-1]
    answer_ids.each do |key, value|
      @answer_ids.push(key)
    end
    @user_ids = []
    user_ids.each do |key, value|
      @user_ids.push(key)
    end
  end
end

