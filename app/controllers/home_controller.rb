class HomeController < ApplicationController
  before_filter :authenticate_user!, only: [:index]

  def index
  end

  def about_research
  end

  def about_media
  end
  
  def welcome
  end
  
  def contact_us
    if request.method == 'POST'
      if params[:contact_us][:content].blank?
        flash.now[:alert] = I18n.t("home.contact_us_msg_1")
      else
        UserMailer.contact_us_email(params[:contact_us][:email], params[:contact_us][:reason], params[:contact_us][:content]).deliver
        flash.now[:alert] = I18n.t("home.contact_us_msg_2")
      end
    end
  end

  def set_language
    session[:locale] = params[:locale]
    I18n.locale = params[:locale]
    redirect_to :back
  end
end
