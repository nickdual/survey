class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale
 
  def set_locale
    I18n.locale = session[:locale] || I18n.default_locale
  end

  def authenticate_admin_user!
    authenticate_user!
    unless current_user.admin?
      flash[:alert] = I18n.t("application.msg_admin_are")
      redirect_to root_path
    end
  end

  def current_admin_user
    return nil if user_signed_in? && !current_user.admin
    current_user
  end
  
  def after_sign_in_path_for(resource)
    if current_user.last_sign_in_at.nil?
      welcome_home_index_path
    else
      root_path
    end
  end
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
  
  private

  def is_number?(str)
    str =~ /^\d+$/
  end
end
