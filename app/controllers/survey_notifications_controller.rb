class SurveyNotificationsController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource except: [:check_new]
  
  def check_new
    render json: {new_length: current_user.survey_notifications.new_msg.size}
  end
end
