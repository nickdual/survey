jQuery ->

  reloadDefaultQuestions = ->
    disply_txts = {
      0: 'Welcome'
      1: 'Qualified'
      2: 'Declined'
    }
    for i in [0..2]
      type_id = i + 11
      $('#survey_survey_questions_attributes_' + i + '_survey_question_type_id').html('<option selected="selected" value="' + type_id + '">' + disply_txts[i] + '</option>')
      $('#survey_survey_questions_attributes_' + i + '_survey_graph_type_input').hide()
      added_div = $('#survey_survey_questions_attributes_' + i + '_survey_question_type_id').parent().parent()
      added_div.next().hide()

  hideAnswerLabels = (div) ->
    div.find('.answer_label').hide()

  hideAllInAnswerNestedForm = (div) ->
    div.find('#answer_text').hide()
    div.find('#answer_english_translation').hide()
    div.find('#answer_special_action').hide()
    div.find('#answer_validation_number_freeform').hide()
    div.find('#answer_validation_text').hide()
    div.find('#answer_next_question').hide()
    div.find('#answer_validation_text_freeform').hide()

  reloadAnswerNestedForm = (question_type, div, cnt) ->
    hideAllInAnswerNestedForm(div)
    answers_part = div.parent()
    hideAnswerLabels(answers_part)
    div.find('#answer_text').show()
    div.find('#answer_text').find('label').hide()
    div.find('#answer_english_translation').show()
    answers_part.find('.answer_label#answer_text_label').text('Text')
    answers_part.find('.answer_label#answer_text_label').show()
    answers_part.find('.answer_label#answer_english_translation_label').show()

    switch question_type
      # Select one
      when '1'
        div.find('#answer_next_question').show()
        answers_part.find('.answer_label#answer_next_question_label').show()

      # Select many
      when '2'
        div.find('#answer_next_question').show()
        answers_part.find('.answer_label#answer_next_question_label').show()

      # Rank order
      when '3'
        if cnt == 1
          div.find('#answer_next_question').show()
        answers_part.find('.answer_label#answer_next_question_label').show()

      # Numeric Input
      when '6'
        div.find('#answer_validation_number_freeform').show()
        div.find('#answer_next_question').show()
        div.find('#answer_english_translation').hide()
        answers_part.find('.answer_label#answer_text_label').text('Number')
        answers_part.find('.answer_label#answer_validation_text_label').text('Operator')
        answers_part.find('.answer_label#answer_english_translation_label').hide()
        answers_part.find('.answer_label#answer_validation_text_label').show()
        answers_part.find('.answer_label#answer_next_question_label').show()

      # Image one
      when '10'
        div.find('#answer_validation_text').show()
        answers_part.find('.answer_label#answer_validation_text_label').text('Image URL')
        answers_part.find('.answer_label#answer_validation_text_label').show()
        div.find('#answer_next_question').show()
        answers_part.find('.answer_label#answer_next_question_label').show()

      # Image many
      when '14'
        div.find('#answer_validation_text').show()
        div.find('#answer_next_question').show()
        answers_part.find('.answer_label#answer_validation_text_label').text('Image URL')
        answers_part.find('.answer_label#answer_validation_text_label').show()
        answers_part.find('.answer_label#answer_next_question_label').show()

    # Text Input
      when '15'
        div.find('#answer_validation_text_freeform').show()
        div.find('#answer_next_question').show()
        div.find('#answer_english_translation').hide()
        answers_part.find('.answer_label#answer_text_label').text('Text')
        answers_part.find('.answer_label#answer_validation_text_label').text('Operator')
        answers_part.find('.answer_label#answer_english_translation_label').hide()
        answers_part.find('.answer_label#answer_validation_text_label').show()

  # change question_type
  assignClickEventToQuestionTypeSelect = ->
    $('select[id$=survey_question_type_id]').change( ->
      question_type = $(this).val()

      answers_part = $(this).parent().parent().next()
      i = 0
      hideAnswerLabels(answers_part)

      answers_part.find('.nested-fields').each(->
        i += 1
        reloadAnswerNestedForm(question_type, $(this), i)
      )
    )

  # click add question
  $('a[data-associations="survey_questions"]').live('click', ->
    added_div = $(this).parent().prev()

    for i in [1..4]
      added_div.find('a[data-associations="survey_possible_answers"]').trigger('click')

    assignClickEventToQuestionTypeSelect()
    return false
  )

  # click add answer
  $('a[data-associations="survey_possible_answers"]').live('click', ->
    question_type = $(this).parent().parent().parent().children().find('select').first().val()
    added_div = $(this).parent().prev()
    reloadAnswerNestedForm(question_type, added_div, $(this).parent().parent().children().length - 4)

    return false
  )

  # click create/update survey
  $('input[name="commit"]').click( ->
    # remove all hidden validation divs
    $('div[id^=answer_validation_]').each(->
      $(this).html('') if $(this).css('display') == 'none'
    )
  )

  # initalize
  reloadDefaultQuestions()
  assignClickEventToQuestionTypeSelect()
  $('select[id$=survey_question_type_id]').trigger('change')

  $('.submit_edit').on('click',(e) ->
    el = $(document).find('div.nested-fields.question')
    $.each(el, (index, div) ->
      div_el = $(div).find('div#answer_text')
      arr_el = []
      arr_text = []
      flag = []
      temp = 0;
      $.each(div_el, (index, value) ->
        answer_text_el = $(value).find('input')
        if answer_text_el.val() != null
          arr_text.push(answer_text_el.val())
          arr_el.push($(value).closest('ol').find('div#answer_text'))
      )
      for i in [0...arr_text.length] by 1
        val = arr_text[i]
        for j in [i+1...arr_text.length] by 1
          if val == arr_text[j]
            for n in [0...flag.length] by 1
              if j == flag[n]
                temp += 1
            if temp == 0
              flag.push(j)
            else
              temp = 0;
      $.each(flag,(index, value) ->
        arr_el[value].closest('div.nested-fields').remove()
      )
    )
  )
