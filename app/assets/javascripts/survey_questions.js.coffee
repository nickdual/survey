jQuery ->
  jQuery('body.surveys_question').each ->
    $question_type = $('#question_type_id').val()
    $other_text = $('#other_text')
    $other_answer_id = $('#other_answer_id').val()
    $result = $('#result')

    jQuery('.button-group > a').click ->
      clicked_button = $(this)

      if $question_type == '1' or $question_type == '10'
        $other_text.hide()
        jQuery('.button-group > a').each ->
          if clicked_button.data('answer-id') != $(this).data('answer-id')
            $(this).removeClass('success')
            $(this).addClass('secondary')

        if clicked_button.hasClass('secondary')
          clicked_button.removeClass('secondary')
          clicked_button.addClass('success')
          $result.val(clicked_button.data('answer-id'))
        else
          clicked_button.removeClass('success')
          clicked_button.addClass('secondary')

      else if $question_type == '2' or $question_type == '14'
        result_array = $result.val().split(',')
        clicked_answer_id = clicked_button.data('answer-id')

        if clicked_button.hasClass('secondary')
          clicked_button.removeClass('secondary')
          clicked_button.addClass('success')
          result_array.push(clicked_answer_id)
        else
          clicked_button.removeClass('success')
          clicked_button.addClass('secondary')
          result_array = jQuery.grep(result_array, (value)-> ( value != clicked_answer_id.toString()))

        result_array = jQuery.grep(result_array, (value)-> ( value != ""))
        $result.val(result_array.join(","))

      # Rank order
      else if $question_type == '3'
        return false if !clicked_button.data('choice')

        if clicked_button.data('choice') == '-'
          cur_button = clicked_button.next()
          next_button = clicked_button.parent().prev().children().first().next()
        else
          cur_button = clicked_button.prev()
          next_button = clicked_button.parent().next().children().first().next()

        if next_button
          content_1 = cur_button.clone()
          content_2 = next_button.clone()
          cur_button.replaceWith(content_2)
          next_button.replaceWith(content_1)

          # reassign rank orders to each possible answer
          i = 0
          jQuery('.button-group > .rank-text').each ->
            i += 1
            $('#text' + $(this).data('answer-id')).val(i)

      # other text
      if clicked_button.data('answer-id') == parseInt($other_answer_id)
        if clicked_button.hasClass('secondary')
          $other_text.hide()
        else
          $other_text.show()

      false

    jQuery('.question-input').keypress((e)->
      if e.which == 13
        $('form.question-for').submit()
        false
    )

    $other_text.hide()

