jQuery ->
  $('.datepicker').datepicker
    autoclose: true
    format: "yyyy-mm-dd"

  # Notification checking frequently
  checkNotification = ->
    $.ajax({
      url: '/survey_notifications/check_new.json'
      contentType: "application/json; charset=utf-8"
      dataType: "json"
      data: {}
      success: (data) ->
        if data['new_length'] != 0
          $('#notification_new_size').show()
          $('#notification_new_size').text(data['new_length'])
        else
          $('#notification_new_size').hide()

    })

  # TODO: login check
  checkNotification()
  setInterval(checkNotification, 30000)

  # Language Tab
  changeLanguage = (lang) ->
    window.location = '/home/set_language?locale=' + lang

  $('.english_tab').click ->
    $('.english_tab').addClass('active')
    $('.espanol_tab').removeClass('active')
    changeLanguage('en')

  $('.espanol_tab').click ->
    $('.espanol_tab').addClass('active')
    $('.english_tab').removeClass('active')
    changeLanguage('es')
    



