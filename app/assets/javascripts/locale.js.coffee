# Question
window.txt_progress_done = {'en': "Done", 'es': "Completado"}
 
# Survey/index
window.txt_survey_text_01 = {'en': "Congratulations!", 'es': "Felicidades!"}
window.txt_survey_text_02 = {'en': "Dear", 'es': "Querido"}
window.txt_survey_text_03 = {'en': "Thank you for your inquiry! Our representative will reach out to you shortly. <br/> Best", 'es': " Gracias por su consulta! Nuestro representante se pondrá en contacto con usted próximamente. <br/> Best"}
 
# Survey/result_filter
window.txt_survey_text_11 = {'en': "Age", 'es': "Edad"}
window.txt_survey_text_12 = {'en': "Remove Filter", 'es': "Quitar Categoría"}
window.txt_survey_text_13 = {'en': "Age must be a number smaller than 200.", 'es': "La edad debe ser un número menor de 200."}
window.txt_survey_text_14 = {'en': "Gender", 'es': "Género"}
window.txt_survey_text_15 = {'en': "Gender must be selected.", 'es': "El género deberá ser seleccionado."}
window.txt_survey_text_16 = {'en': "State", 'es': "Estado"}
window.txt_survey_text_17 = {'en': "City", 'es': "Ciudad"}
window.txt_survey_text_18 = {'en': "Zipcode", 'es': "Código Postal"}
window.txt_survey_text_19 = {'en': "Zipcode must be 5 digits long.", 'es': "El código postal debe tener 5 dígitos."}
window.txt_survey_text_20 = {'en': "From", 'es': "Desde"}
window.txt_survey_text_21 = {'en': "To", 'es': "Hasta"}
window.txt_survey_text_22 = {'en': "Must select from date.", 'es': "Debe seleccionar desde cuándo para la fecha."}
window.txt_survey_text_23 = {'en': "Must select to date.", 'es': " Debe seleccionar hasta cuándo para la fecha."}
window.txt_survey_text_24 = {'en': "From date should be before the to date.", 'es': "Primero seleccione desde cuándo y después seleccione hasta cuándo para la fecha."}
window.txt_survey_text_25 = {'en': "Date", 'es': "Fecha"}
window.txt_survey_text_26 = {'en': "Location", 'es': "Localidad"}
window.txt_survey_text_27 = {'en': "Please enter a state, city or zip code.", 'es': "Por favor, introduzca un estado, ciudad o un código postal."}
 
# Survey/Invite
window.txt_survey_text_41 = {'en': "Invite People", 'es': "Invite a otras personas"}
window.txt_survey_text_42 = {'en': " has invited you to see the results for a new consumer survey.", 'es': " te ha invitado a ver los resultados de la encuesta del consumidor."}
window.txt_survey_text_43 = {'en': "You must input an email.", 'es': "por favor introducir una dirección de correo electrónico."}
window.txt_survey_text_44 = {'en': "Please input a message to send.", 'es': "Por favor, introduzca un mensaje para enviar."}
window.txt_survey_text_45 = {'en': "Great!", 'es': "¡Estupendo!"}
window.txt_survey_text_46 = {'en': "An invitation has been sent to", 'es': "Una invitación ha sido enviada a"}