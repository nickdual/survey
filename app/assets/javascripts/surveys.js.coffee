jQuery ->
  filterItems = new Array()

  jsonConcat = (o1, o2) ->
    for key of o2
      o1[key] = o2[key]
    o1

  $('body.surveys_index').each ->
    if $('#notice').val() == 'The survey was successfully created.'
      $('#alert_modal_header').html(window.txt_survey_text_01[window.locale])
      $('#alert_modal_body').html('<div style="margin-left:10px;"><strong>' +  window.txt_survey_text_02[window.locale] + ' ' + window.user_name + ' </strong> <p> ' + window.txt_survey_text_03[window.locale] + ' </p></div>')
      $('#alert_modal').modal('show')
  
  $('body.surveys_results').each ->
    i = 0
    hideFitlerBoxs = ->
      $('#filter-date').hide()
      $('#filter-age-greator').hide()
      $('#filter-age-less').hide()
      $('#filter-gender').hide()
      $('#filter-location').hide()

    isInt = (n) ->
      return parseInt(n) == parseFloat(n) && !isNaN(n)

    $filter_by = $('#filter_by')
    $('#filter_by').change ->
      hideFitlerBoxs()
      filterItem = $filter_by.val()
      
      switch filterItem
        when 'age','edad' then filterItem = 'age'
        when 'gender','género' then filterItem = 'gender'
        when 'location','localidad' then filterItem = 'location'
        when 'date','fecha' then filterItem = 'date'

      
      if filterItem != ''
        if filterItem == 'age'          
          $('#filter-' + filterItem + '-greator').show()
          $('#filter-' + filterItem + '-less').show() 
        else
          $('#filter-' + filterItem).show()

      $('.filter_item').removeClass('warning') if $('.filter_item').hasClass('warning')
      $('#validation-error').fadeOut('slow') if $('#validation-error').is(':visible')

    $('#filter-' + $filter_by.val()).show()

    $('.add-filter-gender').click ->
      arguments[0].preventDefault()
      i += 1
      filterItemCondition = {}
      filterItem = 'gender'
      errorMessage = ''
      $('.filter_item').removeClass('warning') if $('.filter_item').hasClass('warning')
      $('#validation-error').fadeOut('slow') if $('#validation-error').is(':visible')

      if $(this).hasClass('male')
        gender = "male"
      else  
        gender = "female"

      if $(this).hasClass('active')
        thisId = $(this).attr('data-filter-id');
        console.log $('a.remove-filter#' + thisId)
        $('a.remove-filter#' + thisId).parent().parent().remove()
        $(this).removeClass('active').removeAttr('data-filter-id')
      else
        $(this).addClass('active')
        $(this).attr('data-filter-id', i.toString())

        if $.inArray(gender, ["male, female"]) && gender != ''
          $('#filter-queries > tbody')
            .append("<tr><td><b>" + window.txt_survey_text_14[window.locale] + "</b></td><td colspan=6><b>#{gender}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>")
          filterItemCondition.gender = gender
          filterItems.push(filterItemCondition)
          
        else
          errorMessage = window.txt_survey_text_15[window.locale]
      if errorMessage != ''
        $('.filter_item').addClass('warning')
        $('#validation-error').text(errorMessage)
        $('#validation-error').fadeIn('slow')
      else
        filterItems.push(filterItemCondition)

      $('.remove-filter').click ->
          row = $(this).parent().parent()
          deletedItemId = parseInt($(this).attr('id')) - 1
          filterItems[deletedItemId] = null
          $('a[data-filter-id=' + $(this).attr('id') + ']').removeAttr('data-filter-id').removeClass('active');
          row.remove() 

    $('.add-filter-age').click ->
      arguments[0].preventDefault()
      i += 1
      filterItemCondition = {}
      filterItemCondition.min_age_greator = $(this).data('from')
      filterItemCondition.min_age_less = $(this).data('to') if $(this).data('to')
      filterItem = 'age'
      errorMessage = ''
      $('.filter_item').removeClass('warning') if $('.filter_item').hasClass('warning')
      $('#validation-error').fadeOut('slow') if $('#validation-error').is(':visible')

      if $(this).hasClass('active')
        $('a.remove-filter#'+$(this).attr('data-filter-id')).parent().parent().remove()
        $(this).removeClass('active').removeAttr('data-filter-id')
        
      else
        $(this).addClass('active')
        $(this).attr('data-filter-id', i.toString())
        $('#filter-queries > tbody').append("<tr><td><b>" + window.txt_survey_text_11[window.locale] + "</b></td><td colspan=5>#{$('#filter-age-greator').text()}</td><td><b>#{$(this).data('from')}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>")
        $('#filter-queries > tbody').append("<tr><td><b>" + window.txt_survey_text_11[window.locale] + "</b></td><td colspan=5>#{$('#filter-age-less').text()}</td><td><b>#{$(this).data('to')}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>") if filterItemCondition.min_age_less
      if errorMessage != ''
        $('.filter_item').addClass('warning')
        $('#validation-error').text(errorMessage)
        $('#validation-error').fadeIn('slow')
      else
        filterItems.push(filterItemCondition)

      $('.remove-filter').click ->
          row = $(this).parent().parent()
          deletedItemId = parseInt($(this).attr('id')) - 1
          filterItems[deletedItemId] = null
          $('a[data-filter-id=' + $(this).attr('id') + ']').removeAttr('data-filter-id').removeClass('active');
          row.remove() 

    $('#add-filter').click ->
      i += 1
      filterItemCondition = {}
      filterItem = $filter_by.val()
      errorMessage = ''
      $('.filter_item').removeClass('warning') if $('.filter_item').hasClass('warning')
      $('#validation-error').fadeOut('slow') if $('#validation-error').is(':visible')

      switch filterItem
        when 'age','edad'
          ageValue_greator = $('input[id="filter_age_greator"]').val()
          ageValue_less = $('input[id="filter_age_less"]').val()

          if isInt(ageValue_greator) && Number(ageValue_less) < 200
            $('#filter-queries > tbody')
              .append("<tr><td><b>" + window.txt_survey_text_11[window.locale] + "</b></td><td colspan=5>#{$('#filter-age-greator').text()}</td><td><b>#{ageValue_greator}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>")
            filterItemCondition.min_age_greator = ageValue_greator
          else
            errorMessage = window.txt_survey_text_13[window.locale]
          if  ageValue_less.length > 0
            if (isInt(ageValue_less) ) && Number(ageValue_less) < 200
              $('#filter-queries > tbody')
                .append("<tr><td><b>" + window.txt_survey_text_11[window.locale] + "</b></td><td colspan=5>#{$('#filter-age-less').text()}</td><td><b>#{ageValue_less}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>")
              filterItemCondition.min_age_less = ageValue_less
            else
              errorMessage = window.txt_survey_text_13[window.locale]
        when "gender",'género'
          gender = $('select[id="filter_gender"]').val()
          if $.inArray(gender, ["male, female"]) && gender != ''
            $('#filter-queries > tbody')
              .append("<tr><td><b>" + window.txt_survey_text_14[window.locale] + "</b></td><td colspan=6><b>#{gender}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>")
            filterItemCondition.gender = gender
          else
            errorMessage = window.txt_survey_text_15[window.locale]
        when "location",'localidad'
          state = $('#filter_state').val()
          city  = $('#filter_city').val()
          zipCode = $('#filter_zipcode').val()
          if state == "" && city == "" && zipCode == ""
            errorMessage = window.txt_survey_text_27[window.locale]
          else if zipCode != ''
            if isInt(zipCode) && zipCode.length == 5
              $('#filter-queries > tbody')
                .append("<tr><td><b>" + window.txt_survey_text_26[window.locale] + "</b></td><td>" + window.txt_survey_text_16[window.locale] + "</td><td><b>#{$('#filter_state').attr("value")}</b></td><td>" + window.txt_survey_text_17[window.locale] + "</td><td><b>#{$('#filter_city').attr("value")}</b></td><td> " + window.txt_survey_text_18[window.locale] + "</td><td><b>#{zipCode}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>")
              filterItemCondition.state = $('#filter_state').val()
              filterItemCondition.city = $('#filter_city').val()
              filterItemCondition.zipcode = $('#filter_zipcode').val()
            else
              errorMessage = window.txt_survey_text_19[window.locale]
          else if zipCode == ''
            $('#filter-queries > tbody')
              .append("<tr><td><b>" + window.txt_survey_text_26[window.locale] + "</b></td><td>" + window.txt_survey_text_16[window.locale] + "</td><td><b>#{$('#filter_state').attr("value")}</b></td><td>" + window.txt_survey_text_17[window.locale] + "</td><td><b>#{$('#filter_city').attr("value")}</b></td><td> " + window.txt_survey_text_18[window.locale] + "</td><td><b>#{zipCode}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>")
            filterItemCondition.state = $('#filter_state').val()
            filterItemCondition.city = $('#filter_city').val()
            filterItemCondition.zipcode = $('#filter_zipcode').val()


        when "date",'fecha'
          fromDate = $('#filter_from').val()
          toDate = $('#filter_to').val()
          if fromDate != '' && toDate != '' && fromDate <= toDate
            $('#filter-queries > tbody')
              .append("<tr><td><b>" + window.txt_survey_text_25[window.locale] + "</b></td><td colspan=2>" + window.txt_survey_text_20[window.locale] + "</td><td><b>#{fromDate}</b></td><td colspan=2>" + window.txt_survey_text_21[window.locale] + "</td><td><b>#{toDate}</b></td><td><a class='remove-filter' id='#{i}'>" + window.txt_survey_text_12[window.locale] + "</a></td></tr>")
            filterItemCondition.fromdate = fromDate
            filterItemCondition.todate = toDate           
          else if fromDate == ''
            errorMessage = window.txt_survey_text_22[window.locale]
          else if toDate == ''
            errorMessage = window.txt_survey_text_23[window.locale]
          else if fromDate > toDate
            errorMessage = window.txt_survey_text_24[window.locale]

      if errorMessage != ''
        $('.filter_item').addClass('warning')
        $('#validation-error').text(errorMessage)
        $('#validation-error').fadeIn('slow')
      else
        filterItems.push(filterItemCondition)

      $('.remove-filter').click ->
        row = $(this).parent().parent()
        deletedItemId = parseInt($(this).attr('id')) - 1
        filterItems[deletedItemId] = null
        row.remove()

    $('#result-search').click ->
      console.log filterItems
      filters = {}
      for ind of filterItems        
        filters = jsonConcat(filters, filterItems[ind])
      window.location = "/surveys/#{window.survey_id}/results?filterItems=" + JSON.stringify(filters)

    for question_id in window.question_ids
      if $('#question_graph_type_' + question_id).val() == '1'
        window.drawBasicPieChart('question_chart_' + question_id, JSON.parse($('#question_graph_data_' + question_id).val()))
      else if $('#question_graph_type_' + question_id).val() == '2'
        window.drawBasicColumnChart('question_chart_' + question_id, JSON.parse($('#question_graph_data_' + question_id).val()))
      else if $('#question_graph_type_' + question_id).val() == '3'
        window.drawBasicBarChart('question_chart_' + question_id, JSON.parse($('#question_graph_data_' + question_id).val()))
      else if $('#question_graph_type_' + question_id).val() == '4'
        window.drawBasicBarChart('question_chart_' + question_id, JSON.parse($('#question_graph_data_' + question_id).val()))

  $('body.surveys_share').each ->
    $('#confirm_modal').width('380px')
    $('#confirm_modal_header').html(window.txt_survey_text_41[window.locale])
    $('#confirm_modal_body').html($('#invite_form_div').html())
    $('#invite_form_div').html('')

    $invite_email = $('#invite_email')
    $invite_message = $('#invite_message')

    $('.invite').click ->
      $invite_email.val('')
      $invite_message.val($(this).data('firstname') + ' ' + $(this).data('lastname') + window.txt_survey_text_42[window.locale])
      $('#invite_survey_id').val($(this).data('survey-id'))
      $('#confirm_modal').modal('show')

    $('#confirm_modal_ok').click ->
      if $invite_email.val() == ""
        $('#invite_alert').html(window.txt_survey_text_43[window.locale]).show()
        $invite_email.focus()
        return false

      if $invite_message.val() == ""
        $('#invite_alert').html(window.txt_survey_text_44[window.locale]).show()
        $invite_message.focus()
        return false

      $('#invite_form').submit()

      false

    $("#invite_form").bind("ajax:loading",  ->)
                     .bind("ajax:complete", ->)
                     .bind("ajax:success", (xhr, data, status) ->
      if data.success == true
        #confirm modal hide
        $('#confirm_modal').modal('hide')
        # page redraw
        $('#invite_' + $('#invite_survey_id').val()).load('/surveys/' + $('#invite_survey_id').val() + '/share_list')

        # alert modal show
        $('#alert_modal_header').html(window.txt_survey_text_45[window.locale])
        $('#alert_modal_body').html('<div style="margin-left:10px;"><p> ' + window.txt_survey_text_46[window.locale] + $invite_email.val() + ' </p></div>')
        $('#alert_modal').modal('show')
      else
        $('#invite_alert').html(data.message).show()
        $invite_email.focus()
    )

