//
//= require jquery
//= require jquery.ui.datepicker
//= require plugins/jquery-jvectormap-1.2.2.min
//= require jquery-vector-map
//= require jquery_ujs
//= require bootstrap-modal
//= require underscore
//= require select2
//= require autocomplete-rails
//= require_tree .
