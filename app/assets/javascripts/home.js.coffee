jQuery ->
  AboutMediaSlider =
    init: ->
      $("img.rollover:not(.hoveron)").hover(
        ->
          this.src = this.src.replace("_off","_on")
        ->
          if (!$(this).hasClass('hoveron'))
            this.src = this.src.replace("_on","_off")
      )
      
      $("img.rollover").click(->
        return if ($(this).hasClass('hoveron'))
        
        $("img.rollover.hoveron").each(->
          this.src = this.src.replace("_on","_off")
          $(this).removeClass('hoveron')
        )

        $(this).toggleClass('hoveron')
      )

      $('#coda-slider-1').codaSlider({
        autoSlide: false
        dynamicArrows: false
        dynamicTabs: false
        autoHeight: false
        firstPanelToLoad:1
      })



      $("#about_vision").each(->
        this.src = this.src.replace("_off","_on")
        $(this).removeClass('hoveron')
      )

      $("#about_vision").toggleClass('hoveron')

  jQuery('body.home_about_media').each ->
    $(document).ready ->
      AboutMediaSlider.init()      
  
  jQuery('body.home_contact_us').each ->
    $('#contact_us_reason').select2
      width: '305px'

  jQuery('body.home_index').each ->
    $('#slider').movingBoxes({
      startPanel   : 2
      reducedSize  : 1
      wrap         : true
      buildNav     : true
      fixedHeight  : true
      navFormatter : ->
        return "&#9679;" 
    })
    $('#featured').orbit({
      animation: 'fade'
      animationSpeed: 800
      timer: false
      advanceSpeed: 4000
      pauseOnHover: false
      startClockOnMouseOut: false
      startClockOnMouseOutAfter: 1000
      directionalNav: true
      captions: true
      captionAnimation: 'fade'
      captionAnimationSpeed: 800
      bullets: false
      bulletThumbs: false
      bulletThumbLocation: ''
      
    })
    $('.mb-inside').each( ->
      $(this).css({height: '310px'})
    )
    $('.mb-panel').each( ->
      $(this).css({height: '310px'})
    )



