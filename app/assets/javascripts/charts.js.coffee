jQuery ->
    # Draw Charts
    window.drawBasicPieChart = (render_to, data)->
      new Highcharts.Chart(
        {
          chart: {
            renderTo: render_to
            plotBackgroundColor: null
            plotBorderWidth: null
            plotShadow: false
            margin: [5, 100, 5, 5]
          }
          title: {
            text: ""
          }
          tooltip: {
            pointFormat: '<b>{point.percentage}%</b>'
            percentageDecimals: 1
          }
          plotOptions: {
            pie: {
              size:'100%'
              allowPointSelect: false
              cursor: 'pointer'
              dataLabels: {
                enabled: true
                color: '#000000'
                connectorColor: '#000000'
                distance: -40
                formatter: ->
                  Math.round(this.point.percentage).toFixed(1) + '%'
              }
              showInLegend: true
            }
          }
          legend: {
            layout: 'vertical'
            backgroundColor: '#FFFFFF'
            align: 'left'
            verticalAlign: 'top'
            x: 400
            y: 70
            floating: true
            shadow: true
          }
          series: [
            {
              type: 'pie'
              data: data
            }
          ]
        }
      )

    window.drawBasicColumnChart = (render_to, data)->
      new Highcharts.Chart(
        {
          chart: {
            renderTo: render_to
            type: 'column'
            margin: [50, 100, 50, 50]
          }
          title: {
            text: ""
          }
          tooltip: {
            formatter: ()->
              return '' + this.series.name + ': ' + this.y
          }
          legend: {
            layout: 'vertical'
            backgroundColor: '#FFFFFF'
            align: 'left'
            verticalAlign: 'top'
            x: 400
            y: 70
            floating: true
            shadow: true
          }
          plotOptions: {
            column: {
              pointPadding: 0.2
              borderWidth: 0
            }
          }
          xAxis: {
            categories: data['categories']
            title: {
              text: data['x_text']
            }
          }
          yAxis: {
            min: 0
            title: {
              text: 'Seleted count'
            }
            stackLabels: {
              enabled: true
              style: {
                  fontWeight: 'bold'
                  color: 'gray'
              },
              formatter: ->
                return  this.stack
            }
          }
          series: data['series']
        }
      )
    
    window.drawBasicBarChart = (render_to, data)->
      new Highcharts.Chart(
        {
          chart: {
            renderTo: render_to
            type: 'bar'
            margin: [50, 100, 50, 50]
          }
          title: {
            text: ""
          }
          tooltip: {
            formatter: ()->
              return '' + this.series.name + ': ' + this.y
          }
          legend: {
            layout: 'vertical'
            backgroundColor: '#FFFFFF'
            align: 'right'
            verticalAlign: 'top'
            x: -30
            y: 70
            borderWidth: 1
            floating: true
            shadow: true
          }
          plotOptions: {
            bar: {
              dataLabels: {
                enabled: true
              }
            }
          }
          xAxis: {
            categories: data['categories']
            title: {
              text: data['x_text']
            }
          }
          yAxis: {
            min: 0
            title: {
              text: 'Seleted Count'
            }
          }
          series: data['series']
        }
      )