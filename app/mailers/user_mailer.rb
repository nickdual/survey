class UserMailer < ActionMailer::Base
  default from: "deekauhelp@gmail.com"
  extend ApplicationHelper
  
  def survey_creation_email(user, survey)
    @admin_user = User.find_by_email('deekauhelp@gmail.com') # FIXME
    @user = user
    @survey = survey
    mail to: @admin_user.email, subject: 'A New Survey has been created'
  end
  
  def contact_us_email(email, reason, content)
    @admin_user = User.find_by_email('deekauhelp@gmail.com') # FIXME
    @user = User.find_by_email(email)
    @email = email
    @reason = reason
    @content = content
    mail to: @admin_user.email, subject: "New Message:#{reason}"
  end

  def invite_survey_to_user(user, invited_user, message, survey, new_user_flag)
    @survey = survey
    @user = user
    @invited_user = invited_user
    @invited_user.reset_password_token = User.reset_password_token
    @invited_user.reset_password_sent_at = Time.now
    @invited_user.save
    @link_url = new_user_flag ? edit_user_password_url(reset_password_token: @invited_user.reset_password_token, invited: true) : survey_notifications_url
 mail to: invited_user.email, subject: "#{user.full_name} invites you to view these survey results"
  end
end
