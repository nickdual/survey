ActiveAdmin.register MarketingMaterial do
  menu :if => proc{ can?(:manage, MarketingMaterial) }, priority: 7

  config.sort_order = 'id_asc'
  
  scope :active
  scope :inactive
  
  index do
    selectable_column
    column :title
    column :published_date
    column :image do |material|
      image_tag(material.image_url, size: '180x180')
    end
    column :destination_url do |material|
      link_to material.destination_url, material.destination_url
    end
    column :active do |material|
      status_tag (material.active ? "Active" : "Inactive"), (material.active ? :ok : :warn)
    end
    default_actions
  end
  
  controller do
    before_filter :set_locale
    authorize_resource
    
    def set_locale
      I18n.locale = 'en'
    end
  end
end
