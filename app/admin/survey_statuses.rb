ActiveAdmin.register SurveyStatus do
  menu :if => proc{ can?(:manage, SurveyStatus) }, priority: 10

  controller do
    before_filter :set_locale
    authorize_resource
        
    def set_locale
      I18n.locale = 'en'
    end
  end
end
