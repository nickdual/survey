ActiveAdmin.register Survey do
  menu priority: 3
  filter :title
  scope :active
  scope :inactive

  index do
      selectable_column
      column :title
      column :user do |survey|
        survey.user.to_s
      end
      default_actions

  end

  show title: :title do
    attributes_table_for survey do
      row("Title") { survey.title }
      row("User") { survey.user }
      row("Audience Description") { survey.audience_description }
      row("Description") { survey.description }
      row("Status") { status_tag (survey.active ? "Active" : "Inactive"), (survey.active ? :ok : :warn) }
    end
    puts "c 1"
    panel "Questions" do
      table_for survey.survey_questions do
        column :question do |question|
    puts "c 2"

          "<p>
            <b>Espanol: #{question.text}</b><br/>
            <b>English: #{question.english_translation}</b><br/>
            #{question.survey_question_type.name} <br/>
            #{question.survey_graph_type.name unless [11, 12, 13].include?(question.survey_question_type_id)} <br/>
            #{question.survey_possible_answers.first.try(:next_question).try(:text) if question.survey_question_type == 3}
          </p>
          ".html_safe
        end
        column :answers do |question|
          result = "<table>"
    puts "c 4"

          # Multi choice select one
          if question.survey_question_type.id == 1
            # header
            result += "<tr><th>Text</th><th>English Translation</th><th>Next Question</th></tr>"
            #body
            question.survey_possible_answers.each do |answer|
              result += "<tr><td>#{answer.text}</td><td>#{answer.english_translation}</td><td>#{answer.next_question.try(:text)}</td></tr>"
            end

          # Multi choice select multiple
          elsif question.survey_question_type.id == 2
            # header
            result += "<tr><th>Text</th><th>English Translation</th><th>Next Question</th></tr>"
            #body
            question.survey_possible_answers.each do |answer|
              result += "<tr><td>#{answer.text}</td><td>#{answer.english_translation}</td><td>#{answer.next_question.try(:text)}</td></tr>"
            end

          # Rank order
          elsif question.survey_question_type.id == 3
            # header
            result += "<tr><th>Text</th><th>English Translation</th></tr>"
            #body
            question.survey_possible_answers.each do |answer|
              result += "<tr><td>#{answer.text}</td><td>#{answer.english_translation}</td><td>#{answer.english_translation}</td></tr>"
            end
    puts "c 5"

          # Numeric Freeform Input
          elsif question.survey_question_type.id == 6
            # header
            result += "<tr><th>Operator</th><th>Value</th><th>Next Question</th></tr>"
            #body
            question.survey_possible_answers.each do |answer|
              result += "<tr><td>#{answer.validation}</td><td>#{answer.text}</td><td> #{answer.next_question.try(:text)}</td></tr>"
            end

          # Image Chooser
          elsif question.survey_question_type.id == 10
            # header
            result += "<tr><th>Text</th><th>English Translation</th><th>Image</th><th>Next Question</th></tr>"
            #body
            question.survey_possible_answers.each do |answer|
              result += "<tr><td>#{answer.text}</td><td>#{answer.english_translation}</td><td><img height='32' width='32' src='#{answer.validation}'></td><td>#{answer.next_question.try(:text)}</td></tr>"
            end
    puts "c 6"

          elsif question.survey_question_type.id == 14
            # header
            result += "<tr><th>Text</th><th>English Translation</th><th>Image</th><th>Next Question</th></tr>"
            #body
            question.survey_possible_answers.each do |answer|
              result += "<tr><td>#{answer.text}</td><td>#{answer.english_translation}</td><td><img height='32' width='32' src='#{answer.validation}'></td><td>#{answer.next_question.try(:text)}</td></tr>"
            end
          else question.survey_question_type.id == 11 || question.survey_question_type.id == 12 || question.survey_question_type.id == 13

          end
          result += "</table>"
    puts "c 7"

          result.html_safe
        end
      end
    end
  end

  form partial: 'form'
    puts "c 8"

  action_item only: :show do
    #link_to "Add Survey question", new_admin_survey_question_path(survey_id: survey.id)
  end

  controller do

    before_filter :set_locale
    before_filter :admin
    authorize_resource except: [:index]
    puts "c 9"


    def new
      @survey = Survey.new(params[:survey])

      @survey.survey_questions.build( survey_question_type_id: 11, survey_graph_type_id: 1, text: 'Welcome', english_translation: 'Welcome' )
      @survey.survey_questions.build( survey_question_type_id: 12, survey_graph_type_id: 1, text: 'Qualified', english_translation: 'Qualified' )
      @survey.survey_questions.build( survey_question_type_id: 13, survey_graph_type_id: 1, text: 'Declined', english_translation: 'Declined' )
      @survey.survey_questions.build( survey_question_type_id: 1, survey_graph_type_id: 1, text: 'First Question', english_translation: 'First Question', order: 1 )
    end

    def set_locale
      I18n.locale = 'en'
    end

    def admin
      if current_user['admin'] || current_user.admin
        return true
      else
        redirect_to '/surveys', :notice =>  I18n.t("surveys.msg_authenticate_failed")
      end
    end
  end

end
