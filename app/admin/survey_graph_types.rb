ActiveAdmin.register SurveyGraphType do
  menu :if => proc{ can?(:manage, SurveyGraphType) }, :priority => 6, label: 'Graph Types'
  
  config.sort_order = 'id_asc'
  
  index do
    selectable_column
    column :id
    column :name
    column :description
    default_actions
  end
  
  controller do
    before_filter :set_locale
    authorize_resource
    
    def set_locale
      I18n.locale = 'en'
    end
  end
end
