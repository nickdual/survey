ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do
    #div :class => "blank_slate_container", :id => "dashboard_default_message" do
      
    #end
    
    columns do
      column do
        panel "Summary Info" do
          para "Total Surveys: #{Survey.all.size}"
        end
      end
      
      column do
        panel "Recent Surveys" do
          ul do
            Survey.last(5).map do |survey|
              li link_to(survey.title, admin_survey_path(survey))
            end
          end
        end
      end
      
    end
  end # content
  
  controller do
    before_filter :set_locale
    def set_locale
      I18n.locale = 'en'
    end
  end
end
