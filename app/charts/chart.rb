class Chart
	attr_reader :results
  
	def initialize
		@results = Array.new
	end
  
	def results=(r)
		@results = r
	end
  
  # It will be overrided in the decendent classes
  def complete_chart_data(survey_question, lang)
    # [].to_json
  end
end