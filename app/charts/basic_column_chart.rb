class BasicColumnChart < Chart
	
	def initialize
		super
	end

	def generate_data(survey_question, lang, answer_ids)
		case survey_question.survey_question_type_id
    when 2, 14 then # Multi choice select multi
      categories = I18n.t("survey_common.selected")
      series = []
      survey_question.survey_possible_answers.includes(:survey_user_answers).each do |answer|
        if answer.other?
          u_answers = answer.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"])
          if u_answers.any?
            u_answers.group_by{ |a| a.text }.each do |a, as|
              #categories << "Other: #{a}"
              series << {name: "#{answer.text}: #{a}", data: [as.size]}
            end
          else
            series << {name: I18n.t("survey_common.other"), data: [0]}
          end
        else
          #categories << answer.text
          series << {name: answer.text_from_lang(lang), data: [answer.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"]).size]}
        end
      end
      @results = {categories: categories, series: series, x_text: survey_question.text_from_lang(lang)}
      
    when 3 then # Rank order
      answers = survey_question.survey_possible_answers
      categories = answers.map(&:text)
      series = []
      cnt = answers.size
      
      tmp_sum = {}
      answers.each { |a| tmp_sum[a] = 0}
      
      answers.size.times do
        data = []
        
        answers.includes(:survey_user_answers).each do |answer|
          selected = answer.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"]).where(text: cnt.to_s).size
          data << selected
          tmp_sum[answer] += cnt * selected
        end
        #series << {name: "Rank #{cnt}", data: data}
        cnt -= 1
      end
      
      series << {name: I18n.t("survey_common.average"), data: tmp_sum.map{|n, v| (v.to_f / n.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"]).size).round(2) } }
      
      @results = {categories: categories, series: series, x_text: I18n.t("survey_common.answers")}
      
    when 6 then # Numeric Freeform Input
      categories = [survey_question.text_from_lang(lang)]
      series = []
      
      answers = survey_question.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"])
      min = answers.minimum(:text).to_i
      max = answers.maximum(:text).to_i
      dist = (max - min) / 5
      
      cnt = 4
      tmp_sum = 0
      5.times do
        start_point = min + dist * cnt
        start_point = min - 1 if cnt == 0
        end_point = min + dist * (cnt + 1)
        end_point = max if cnt == 4
        data = []
        real_start_point = cnt == 0 ? start_point - 1 : start_point
        data << answers.select{|a| (a.text.to_f <= end_point and a.text.to_f > real_start_point)}.size
        series << {name: "#{start_point} ~ #{end_point}", data: data}
        cnt -= 1
      end
      
      #value_array = answers.map(&:text)
      #series << {name: "Average", data: value_array.inject{ |sum, el| sum + el }.to_f / value_array.size }
      
      @results = {categories: I18n.t("survey_common.value"), series: series, x_text: survey_question.text_from_lang(lang)}
    when 15 then
      answers = SurveyUserAnswer.survey_question_eq(survey_question.id).group_by(&:text).where(["survey_user_answers.id in (#{answer_ids.join(', ')})"]).sort_by {|key, value| value.count}.reverse
      @results_raw = []
      answers[0..3].each do |index, value|
        @results_raw.push([index, value.length])
      end
      other_total = 0
      if answers[4..answers.length].present?
        answers[4..answers.length].each do |index, value|
          other_total += value.length
        end
      end
      @results_raw.push(['Other', other_total]) if (other_total > 0)
      @results = {"categories" => "Value","series" => [
      ],"x_text" => nil}
      @results_raw.each do |index, value|
        @results['series'].push({"name" => index, "data" => [value]})
      end
    else
      answer_rate_array = []
      survey_question.survey_possible_answers.includes(:survey_user_answers).each do |choice|
        answer_rate_array << [choice.text_from_lang(lang), choice.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"]).size]
      end
      @results = answer_rate_array
    end
	end
end