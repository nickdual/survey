class BasicPieChart < Chart
	
	def initialize
		super
	end

	def generate_data(survey_question, lang, answer_ids)
		case survey_question.survey_question_type_id
    when 1, 10 then # Multi choice select one
      answer_rate_array = []
      survey_question.survey_possible_answers.includes(:survey_user_answers).each do |answer|
        if answer.other?
          u_answers = answer.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"])
          if u_answers.any?
            u_answers.group_by{ |a| a.text }.each do |a, as|
              answer_rate_array << ["#{answer.text}: #{a}", as.size]
            end
          else
            answer_rate_array << [I18n.t("survey_common.other"), 0]
          end
        else
          answer_rate_array << [answer.text_from_lang(lang), answer.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"]).size]
        end
      end
      @results = answer_rate_array
    when 15 then
      answers = SurveyUserAnswer.survey_question_eq(survey_question.id).where(["survey_user_answers.id in (#{answer_ids.join(', ')})"]).group_by(&:text).sort_by {|key, value| value.count}.reverse
      @results = []
      answers[0..3].each do |index, value|
        @results.push([index, value.length])
      end
      other_total = 0
      if answers[4..answers.length].present?
        answers[4..answers.length].each do |index, value|
          other_total += value.length
        end
      end
      @results.push(['Other', other_total]) if (other_total > 0)
    else
      answer_rate_array = []
      survey_question.survey_possible_answers.includes(:survey_user_answers).each do |choice|
        answer_rate_array << [choice.text, choice.survey_user_answers.where(["survey_user_answers.id in (#{answer_ids.join(', ')})"]).size]
      end
      @results = answer_rate_array
    end
	end
end