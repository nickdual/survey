class SurveyNotification < ActiveRecord::Base
  attr_accessible :text, :user_id, :survey_id, :new_flag, :english_translation, :links => [].to_json

  #validates_uniqueness_of :user_id
  
  belongs_to :user
  belongs_to :survey

  scope :new_msg, where(new_flag: true)
  scope :old_msg, where(new_flag: false)

  # get text with language
  def text_from_lang(lang)
    lang.to_s.downcase == "en" ? self.english_translation : self.text
  end

  def add_link(text_en, text_es, link)
    the_links = get_links
    the_links.reject! {|elm| elm['url'] == link}
    the_links << {"url" => link, "text" => {"en" => text_en, "es" => text_es}}
    self.links = the_links.to_json
    self.save
  end

  def update_link(text_en, text_es, link)
    self.add_link(text_en, text_es, link)
  end

  def delete_link(link)
    the_links = get_links
    the_links.reject! {|elm| elm['url'] == link}
    self.links = the_links.to_json
    self.save
  end

  def has_links?
    get_links.length > 0 ? true : false
  end

  def get_links
    if self.links != nil
      JSON.parse self.links
    else 
      self.links = [].to_json
      self.save
      JSON.parse self.links
    end
  end

  def clear_links
    self.links = [].to_json
    self.save
  end
end
