class User < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include ActionView::Helpers


  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :admin
  attr_accessible :first_name, :last_name, :city, :state, :zip, :phone_number,
                  :address_line_1, :address_line_2, :gender, :age, :role, :user_extension_attributes

  # validation
  validates_presence_of :phone_number, :gender, :age, :zip, if: "dummy_user?"
    validates_numericality_of :age, less_than_or_equal_to: 100, greater_than_or_equal_to: 1, allow_blank: true, if: "dummy_user?"
      validates_numericality_of :zip, less_than_or_equal_to: 99999, greater_than_or_equal_to: 10000, allow_blank: true, if: "dummy_user?"
        validates_uniqueness_of :phone_number, if: "dummy_user?"

          validates_format_of :phone_number, with: /^[\(\)0-9\- \+\.]{10,20}$/i, message: I18n.t("users.val_msg_phone_number"), allow_blank: true
  
          has_many :surveys
          has_many :survey_user_permissions, dependent: :destroy
          has_many :survey_user_answers
          has_many :survey_notifications, dependent: :destroy
          has_many :survey_completes, dependent: :destroy
          has_one :user_extension, dependent: :destroy

          accepts_nested_attributes_for :user_extension, :reject_if => :all_blank

          after_create do
            if self.admin?
              welcome_message = SurveyNotification.create(user_id: self.id, text: "Bienvenidos a la plataforma de estudios de mercado de SABEResPODER!&nbsp;", english_translation: "Welcome to the SABEResPODER Research platform!&nbsp;")
              welcome_message.add_link "My Surveys", "Mis Encuestas", surveys_path
            end
          end
  
          def to_s
            self.full_name
          end

          def full_name
            "#{first_name} #{last_name}"
          end

          def shared_surveys
            @shared_surveys ||= Survey.where(id: self.survey_user_permissions.map(&:survey_id))
          end

          def invitable_shared_surveys
            @invitable_shared_surveys ||= Survey.where(id: self.survey_user_permissions.invitable.map(&:survey_id))
          end

          def editable_shared_surveys
            @editable_shared_surveys ||= Survey.where(id: self.survey_user_permissions.editable.map(&:survey_id))
          end

          def admin?
            self.role == "admin" || self.role == "client"
          end

          def super_admin?
            self.role == "admin"
          end
          def admin

          end

          def dummy_user?
            not self.admin?
          end

          def own_survey?(survey)
            surveys.where(id: survey.id).first.nil? ? false : true
          end

          protected
  
          def password_required?
            false
          end
        end
