class UserExtension < ActiveRecord::Base
  attr_accessible :company, :company_role, :user_id
  
  belongs_to :user
end
