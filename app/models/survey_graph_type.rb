class SurveyGraphType < ActiveRecord::Base
  attr_accessible :description, :name

  validates_presence_of :name, :description
  validates_uniqueness_of :name

  has_many :survey_questions
  
  def to_s
    name
  end
end
