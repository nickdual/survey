class SurveyQuestionType < ActiveRecord::Base
  attr_accessible :description, :name
  
  validates_presence_of :name, :description
  validates_uniqueness_of :name

  has_many :survey_questions
  
  def to_s
    name
  end

  # welcome, qualified, decliend will be not used as default.
  def self.usable_types
  	where('id not in (?)', [11, 12, 13])
  end
end
