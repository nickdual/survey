class Survey < ActiveRecord::Base
  attr_accessible :audience_description, :description, :document, :title, :user_id, :active, :goal
  attr_accessible :survey_questions_attributes
  
  mount_uploader :document, DocumentUploader

  validates_presence_of :user_id, :title, :audience_description, :description
  validates_uniqueness_of :title

  default_scope order('id ASC')
  
  belongs_to :user
  
  has_many :survey_questions, dependent: :destroy
  has_many :survey_user_permissions, dependent: :destroy
  has_many :survey_notifications, dependent: :destroy
  has_many :survey_completes, dependent: :destroy

  accepts_nested_attributes_for :survey_questions, :allow_destroy => true, :reject_if => :all_blank
  
  scope :active, where(active: true)
  scope :inactive, where(active: false)

  def to_s
    title
  end
  
  def survey_possible_answers
    @survey_possible_answers ||= SurveyPossibleAnswer.includes(:survey_question).where(survey_question_id: self.survey_question_ids)
  end

  def survey_user_answers
    @survey_user_answers ||= SurveyUserAnswer.includes(:survey_possible_answer).where(survey_possible_answer_id: self.survey_possible_answers.map(&:id))
  end

  def generate_token(user)
    Digest::MD5.hexdigest("#{user.id}#{user.created_at}#{id}#{created_at}")
  end
end
