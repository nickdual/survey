class Ability
  include CanCan::Ability

  def initialize(user)
    if user.super_admin?
      can :manage, :all
    elsif user.admin?
      can :manage, Survey, :id => user.surveys.pluck(:id) + user.shared_surveys.pluck(:id)
      can :manage, SurveyQuestion
      can :manage, SurveyPossibleAnswer
      #can :manage, SurveyUserAnswer
      can :manage, SurveyNotification, :survey_id => user.surveys.pluck(:id) + user.shared_surveys.pluck(:id)
    else
      can :manage, SurveyUserAnswer
    end
  end
end
