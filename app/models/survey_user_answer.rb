class SurveyUserAnswer < ActiveRecord::Base
  attr_accessible :survey_possible_answer_id, :text, :user_id

  validates_presence_of :survey_possible_answer_id, :user_id
  #validate :validate_uniqueness_of_user_id

  default_scope order('id ASC')

  belongs_to :survey_possible_answer
  belongs_to :user
  
  delegate :survey, :to => :survey_possible_answer, :allow_nil => true
  delegate :survey_question, :to => :survey_possible_answer, :allow_nil => true

  search_methods :survey_question_eq, :survey_eq

  def validate_uniqueness_of_user_id
    if SurveyUserAnswer.where(user_id: user_id, survey_possible_answer_id: survey_question.survey_possible_answers.map(&:id)).size > 0
      errors.add(:user_id, I18n.t("survey_user_answers.val_msg_answer"))
    end
  end

  class << self
    def survey_question_eq(survey_question_id)
      SurveyUserAnswer.where(survey_possible_answer_id: SurveyQuestion.find(survey_question_id).survey_possible_answers.map(&:id)).scoped
    end

    def survey_eq(survey_id)
      SurveyUserAnswer.where(survey_possible_answer_id: Survey.find(survey_id).survey_possible_answers.map(&:id)).scoped
    end
  end

end
