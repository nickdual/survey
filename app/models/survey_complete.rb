class SurveyComplete < ActiveRecord::Base
  attr_accessible :done_by_eo, :notes, :survey_id, :survey_status_id, :user_id

  belongs_to :user
  belongs_to :survey
  belongs_to :survey_status
end
