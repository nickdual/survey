class State < ActiveRecord::Base

  attr_accessible :name
  has_many :zipcodes
  has_many :users
end
