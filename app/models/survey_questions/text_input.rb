module SurveyQuestions
  class SurveyQuestions::TextInput < SurveyQuestion
    OPERATORS = ['==', '!=']
    def validate?(params)

      if params[:text].blank?
        self.errors.add(:base, I18n.t("survey_questions.val_msg_select_answer"))
        return false
      end
      true
    end

    def submit(user, params, save_flag)
      possible_answer = nil
      self.survey_possible_answers.each do |spa|
        if check_rule(params[:text], spa)
          possible_answer = spa
          break
        end
      end
      return if possible_answer.nil?

      if save_flag
        user_answer = possible_answer.survey_user_answers.build(user_id: user.id)
        user_answer[:text] = params[:text]
        user_answer.save
      end

      possible_answer.next_question
    end

    private

    def check_rule(text, possible_answer)
      return_val = false
      case possible_answer.validation
        when '==' then
          return_val = true if text == possible_answer.text
        when '!=' then
          return_val = true if text != possible_answer.text
      end
      return_val
    end

  end
end