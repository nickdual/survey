module SurveyQuestions
  class SurveyQuestions::NumericInput < SurveyQuestion
    OPERATORS = ['==', '>', '>=', '=<', '<']
    
    def validate?(params)
      if params[:text].blank?
        self.errors.add(:base, I18n.t("survey_questions.val_msg_input_answer"))
        return false
      end
      
      if !(params[:text] =~ /^[0-9]+$/)
        self.errors.add(:base, I18n.t("survey_questions.val_msg_input_numeric"))
        return false
      end
      
      true
    end
    
    def submit(user, params, save_flag)
      possible_answer = nil
      self.survey_possible_answers.each do |spa|
         if check_rule(params[:text].to_i, spa)
           possible_answer = spa
           break
         end
      end
      return if possible_answer.nil?
      
      if save_flag
        user_answer = possible_answer.survey_user_answers.build(user_id: user.id)
        user_answer[:text] = params[:text]
        user_answer.save
      end
      
      possible_answer.next_question
    end
    
    private
    
    def check_rule(num, possible_answer)
      return_val = false
      case possible_answer.validation
      when '==' then
        return_val = true if num == possible_answer.text.to_i
      when '<' then
        return_val = true if num < possible_answer.text.to_i
      when '>' then
        return_val = true if num > possible_answer.text.to_i
      when '=<' then
        return_val = true if num <= possible_answer.text.to_i
      when '>=' then
        return_val = true if num >= possible_answer.text.to_i
      end
      return_val
    end
    
  end
end