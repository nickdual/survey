module SurveyQuestions
  class SurveyQuestions::SelectOne < SurveyQuestion
    def validate?(params)
      if params[:result].blank?
        self.errors.add(:base, I18n.t("survey_questions.val_msg_select_answer"))
        return false
      end
      
      if params[:result].to_i == self.other_answer_id && params[:other_text].blank?
        self.errors.add(:base, I18n.t("survey_questions.val_msg_input_other"))
        return false
      end
      
      true
    end
    
    def submit(user, params, save_flag)
      possible_answer = SurveyPossibleAnswer.find_by_id(params[:result])
      
      if save_flag
        user_answer = possible_answer.survey_user_answers.build(user_id: user.id)
        user_answer[:text] = params[:other_text] if possible_answer.other?
        user_answer.save 
      end
      
      possible_answer.next_question
    end
  end
end