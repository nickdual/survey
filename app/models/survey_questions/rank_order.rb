module SurveyQuestions
  class SurveyQuestions::RankOrder < SurveyQuestion
    def validate?(params)
      if params[:text].values.uniq.size < self.survey_possible_answers.size
        self.errors.add(:base, I18n.t("survey_questions.val_msg_rank_dup"))
        return false
      end
      true
    end
    
    def submit(user, params, save_flag)
      next_question = nil
      self.survey_possible_answers.each do |possible_answer|
        if save_flag
          user_answer = possible_answer.survey_user_answers.build(user_id: user.id)
          user_answer[:text] = params[:text][possible_answer.id.to_s]
          user_answer.save 
        end
        
        next_question = possible_answer.next_question if possible_answer.next_question.present?
      end
      
      next_question
    end
  end
end