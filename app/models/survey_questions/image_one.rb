module SurveyQuestions
  class SurveyQuestions::ImageOne < SurveyQuestion
    def validate?(params)
      if params[:result].blank?
        self.errors.add(:base, I18n.t("survey_questions.val_msg_select_image"))
        return false
      end
      
      true
    end
    
    def submit(user, params, save_flag)
      possible_answer = SurveyPossibleAnswer.find_by_id(params[:result])
      
      if save_flag
        user_answer = possible_answer.survey_user_answers.build(user_id: user.id)
        user_answer.save
      end
      
      possible_answer.next_question
    end
    
  end
end