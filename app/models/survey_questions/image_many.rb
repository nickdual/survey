module SurveyQuestions
  class SurveyQuestions::ImageMany < SurveyQuestion
    def validate?(params)
      if params[:result].blank?
        self.errors.add(:base, I18n.t("survey_questions.val_msg_select_answer"))
        return false
      end
      
      true
    end
    
    def submit(user, params, save_flag)
      next_question = nil
      params[:result].split(',').each do |possible_answer_id|
        possible_answer = SurveyPossibleAnswer.find_by_id(possible_answer_id)
        
        if save_flag
          user_answer = possible_answer.survey_user_answers.build(user_id: user.id)
          user_answer.save
        end
        
        next_question = possible_answer.next_question if possible_answer.next_question.present? and next_question.nil?
      end
      
      next_question
    end
    
  end
end