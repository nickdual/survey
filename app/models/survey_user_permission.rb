class SurveyUserPermission < ActiveRecord::Base
  attr_accessible :edit, :invite, :survey_id, :user_id

  validates_presence_of :survey_id, :user_id
  
  scope :invitable, where(invite: true)
  scope :editable, where(edit: true)

  belongs_to :user
  belongs_to :survey
end
