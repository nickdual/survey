class SurveyQuestion < ActiveRecord::Base

  attr_accessible :survey_graph_type_id, :other_answer_id, :survey_id, :survey_question_type_id, :text, :validation, :english_translation, :order
  attr_accessible :survey_possible_answers_attributes
  attr_accessor :step_number

  validates_presence_of :survey_question_type_id, :text, :english_translation

  default_scope order("survey_questions.order ASC")
  
  belongs_to :survey
  belongs_to :survey_question_type
  belongs_to :survey_graph_type
  
  has_many :survey_possible_answers, dependent: :destroy
  has_many :survey_user_answers, through: :survey_possible_answers

  accepts_nested_attributes_for :survey_possible_answers, :allow_destroy => true, :reject_if => :all_blank
  
  # Callback methods
  before_validation do
    # set other_answer_id
    self.other_answer_id = self.other_answer_id.to_i == 0 ?  nil : survey_possible_answers.last.try(:id)
  end

  def to_s
    case survey_question_type_id
    when 1 then
      "#{text} (Select One)"
    when 2 then
      "#{text} (Select Many)"
    when 11 then
      "#{survey_question_type}"
    when 12 then
      "#{survey_question_type}"
    when 13 then
      "#{survey_question_type}"
    else
      text
    end
  end

  def graph_data(lang, answer_ids)
    case survey_graph_type_id
    when 1 then
      chart = BasicPieChart.new()
    when 2 then
      chart = BasicColumnChart.new()
    when 3 then
      chart = BarChart.new()
    when 4 then
      chart = ColumnDistributionChart.new()
    else
      chart = PieChart.new()
    end

    chart.generate_data(self, lang, answer_ids)
    chart.results.to_json
  end

  # We didn't use STI, because of non-changable DB.
  # Just simulate it here :(
  def convert_to_child_class
    case self.survey_question_type_id
    when 1 then
      self.becomes(SurveyQuestions::SelectOne)
    when 2 then
      self.becomes(SurveyQuestions::SelectMany)
    when 3 then
      self.becomes(SurveyQuestions::RankOrder)
    when 4 then
      self.becomes(SurveyQuestions::ConstantSum)
    when 6 then
      self.becomes(SurveyQuestions::NumericInput)
    when 10 then
      self.becomes(SurveyQuestions::ImageOne)
    when 11 then
      self.becomes(SurveyQuestions::Welcome)
    when 12 then
      self.becomes(SurveyQuestions::Qualified)
    when 13 then
      self.becomes(SurveyQuestions::Declined)
    when 14 then
      self.becomes(SurveyQuestions::ImageMany)
    when 15 then
      self.becomes(SurveyQuestions::TextInput)
    else
      self
    end
  end

  # it will be override in the children classes
  def validate?(params)
    false
  end

  # it will be override in the children classes
  def submit(user, params, save_flag = false)
    nil
  end

  # get text with language
  def text_from_lang(lang)
    lang.to_s == "en" ? self.english_translation : self.text
  end

end
