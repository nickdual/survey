class SurveyPossibleAnswer < ActiveRecord::Base
  attr_accessible :next_question_id, :special_action, :survey_question_id, :text, :validation, :english_translation
  
  default_scope order('id ASC')  
  
  validates_presence_of :text
  validates_presence_of :english_translation, if: "has_english_text?"

  belongs_to :survey_question
  
  #has_one :next_question, class_name: 'SurveyQuestion', foreign_key: :next_question_id
  has_many :survey_user_answers, dependent: :destroy
  
  delegate :survey, :to => :survey_question, :allow_nil => true

  def to_s
    "#{text}"
  end

  def has_english_text?
    if SurveyQuestions::NumericInput::OPERATORS.include?(validation) || SurveyQuestions::TextInput::OPERATORS.include?(validation)
      false
    else
      true
    end
  end

  def next_question
    @next_question ||= SurveyQuestion.find_by_id(next_question_id)
  end

  def other?
    survey_question.other_answer_id == self.id
  end

  # get text with language
  def text_from_lang(lang)
    lang.to_s == "en" ? self.english_translation : self.text
  end
end
