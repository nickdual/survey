class MarketingMaterial < ActiveRecord::Base
  attr_accessible :active, :destination_url, :image_url, :published_date, :title

  validates_presence_of :title, :published_date, :image_url

  scope :active, where(active: true).order("created_at desc")
  scope :inactive, where(active: false).order("created_at desc")
end
