@javascript

Feature: Merchant_admin
  Background:
    Given I have a user and login
    
  Scenario Outline: I am in survey page and i want to create a new survey
    Given I am on survey page
    And I click "Create new survey"
    Then I should see "Create Survey"
    And I fill in "survey_title" with "<title>"
    And I fill in "survey_audience_description" with "<Audience description>"
    And I fill in "survey_description" with "<Description>"
    And I click "Create Survey"
    And I wait for 3 seconds
    And I should see "OK"
    And I click "alert_modal_ok"

    Examples:
      | title           | Audience description    |Description |
      | Test survey     | Test hello              | test world |

  Scenario: I am edit a survey
    Given I have a survey
    And I go to survey page with admin user
    And I should see "Hello"
    And I click "Edit"
