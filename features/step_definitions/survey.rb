

def create_visitor
  @visitor ||= { :name => "Test user", :email => "abc@example.com",
                 :password => "123456", :password_confirmation => "123456" }
end
def create_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:user, email: @visitor[:email],password: @visitor[:password])
end
def delete_user
  @user ||= User.first conditions: {:email => @visitor[:email]}
  @user.destroy unless @user.nil?
end
def sign_in
  visit '/users/sign_in'
  fill_in "user_email", :with => @visitor[:email]
  fill_in "user_password", :with => @visitor[:password]
  click_button "Sign In"
end
Given /^I have a survey$/ do
  @survey = FactoryGirl.create(:survey)
end
Given /^I have a user and login/ do
  create_user
  sign_in
end

Given /^I am on survey page/ do
  visit '/surveys'
  end
Given /^I go to survey page with admin user/ do
  visit '/admin/surveys'
end
When /^(?:|I )click "([^"]*)"$/ do |button|
  click_on(button)
end
Then /^I should see "([^"]*)"$/ do |content|
  page.should have_content(content)
end
Then /^I fill in "(.*?)" with "(.*?)"$/ do |field, value|
  fill_in(field, :with => value)
end
When /^I should see a popup and accept it$/ do
  page.driver.browser.switch_to.alert_modal.accept
end
When /^I wait for (\d+) seconds?$/ do |secs|
  sleep secs.to_i
end


