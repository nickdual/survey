FactoryGirl.define do
  factory :survey_possible_answer, :class => 'SurveyPossibleAnswer' do  |f|
    f.id 1
    f.survey_question_id 1
    f.text '123'
    f.validation '=='
    f.next_question_id 1
  end
end
