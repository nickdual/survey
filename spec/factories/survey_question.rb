FactoryGirl.define do
  factory :survey_question, :class => 'SurveyQuestion' do  |f|
    f.id 1
    f.survey_id 1
    f.survey_question_type_id 15
    f.survey_graph_type_id 1
    f.text "Text Question"
    f.validation "f"
    f.english_translation "Text Question"
    f.order 0
  end
  factory :survey_question_number, :class => 'SurveyQuestion' do  |f|
    f.id 2
    f.survey_id 1
    f.survey_question_type_id  6
    f.survey_graph_type_id  1
    f.text  "Number Question"
    f.validation "f"
    f.order  0
  end
  factory :survey_question_multi, :class => 'SurveyQuestion' do  |f|
    f.id  3
    f.survey_id  1
    f.survey_question_type_id  1
    f.survey_graph_type_id  1
    f.text  "Multi Question"
    f.validation  "f"
    f.english_translation  "Multi Question"
    f.order  0
  end
  factory :survey_question_multi_choice, :class => 'SurveyQuestion' do  |f|
    f.id  4
    f.survey_id  1
    f.survey_question_type_id  1
    f.survey_graph_type_id 1
    f.text "Multi Choice"
    f.validation "f"
    f.english_translation  "Multi Choice"
    f.order  0
  end
end
