FactoryGirl.define do
  factory :survey_question_type_1, :class => 'SurveyQuestionType' do  |f|
    f.id  1
    f.name  "Multiple-choice(Select One)"
    f.description  "Multiple-choice(Select One)"
  end
  factory :survey_question_type_2, :class => 'SurveyQuestionType' do  |f|
    f.id  2
    f.name  "Multiple-choice(Select Multiple)"
    f.description  "Multiple-choice(Select Multiple)"
  end
  factory :survey_question_type_3, :class => 'SurveyQuestionType' do  |f|
    f.id  3
    f.name  "Rank order"
    f.description "Rank order"
  end
  factory :survey_question_type_4, :class => 'SurveyQuestionType' do  |f|
    f.id  4
    f.name  "Constant sum"
    f.description  "Constant sum"
  end
  factory :survey_question_type_5, :class => 'SurveyQuestionType' do  |f|
    f.id  5
    f.name  "Drop-down menu"
    f.description  "Drop-down menu"
  end
  factory :survey_question_type_6, :class => 'SurveyQuestionType' do  |f|
    f.id  6
    f.name  "Numeric Freeform Input"
    f.description  "Numeric Freeform Input"
  end
  factory :survey_question_type_7, :class => 'SurveyQuestionType' do  |f|
    f.id  7
    f.name  "Comment Box"
    f.description  "Comment Box"
  end
  factory :survey_question_type_8, :class => 'SurveyQuestionType' do  |f|
    f.id  8
    f.name  "Matrix multi-point scales select one"
    f.description "Matrix multi-point scales select one"
  end
  factory :survey_question_type_9, :class => 'SurveyQuestionType' do  |f|
    f.id  9
    f.name  "Matrix table multi-select"
    f.description "Matrix table multi-select"
  end
  factory :survey_question_type_10, :class => 'SurveyQuestionType' do  |f|
    f.id 10
    f.name  "Image chooser(Select One)"
    f.description  "Image chooser(Select One)"
  end
  factory :survey_question_type_11, :class => 'SurveyQuestionType' do  |f|
    f.id  11
    f.name "Welcome"
    f.description "Welcome"
  end
  factory :survey_question_type_12, :class => 'SurveyQuestionType' do  |f|
    f.id 12
    f.name  "Qualified"
    f.description "Qualified"
  end
  factory :survey_question_type_13, :class => 'SurveyQuestionType' do  |f|
    f.id  13
    f.name  "Declined"
    f.description  "Declined"
  end
  factory :survey_question_type_14, :class => 'SurveyQuestionType' do  |f|
    f.id  14
    f.name "Image chooser(Select Many)"
    f.description "Image chooser(Select Many)"
  end
  factory :survey_question_type_15, :class => 'SurveyQuestionType' do  |f|
    f.id  15
    f.name "Text Freeform Input"
    f.description "Text Freeform Input"
  end

end
