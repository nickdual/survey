FactoryGirl.define do
  factory :survey, :class => 'Survey' do  |s|
    s.id 1
    s.title "Hello"
    s.audience_description "Hello World"
    s.description "Hello World"
    s.active "TRUE"
    s.user_id 1
    end
  factory :survey_hash, :class => Hash do  |s|
    s.id 1
    s.title "Hello"
    s.audience_description "Hello World"
    s.description "Hello World"
    s.active "TRUE"
    s.user_id 1
  end
end
