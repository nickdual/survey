FactoryGirl.define do
  factory :user, :class => 'User' do  |f|
    f.id 1
    f.email "abc@gmail.com"
    f.password "123456"
    f.password_confirmation "123456"
    f.first_name "hello"
    f.last_name "world"
    f.city  "City"
    f.phone_number  "0123456789"
    f.admin  "TRUE"
    f.role  "admin"
  end
end