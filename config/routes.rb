SurveyAdmin::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  devise_for :users

  resources :home, only: [:index] do
    get :welcome, on: :collection
    get :about_research, on: :collection
    get :about_media, on: :collection
    match :contact_us, on: :collection
    match :set_language, on: :collection    
  end
  
  resources :surveys do
    match :results
    match :question
    match :question_info
    match :question_lang
    get   :share, on: :collection
    get   :share_list
    post  :invite, on: :collection
    get :autocomplete_state_name, :on => :collection
  end
  
  resources :survey_notifications do
    get :check_new, on: :collection
  end

  root :to => 'home#index'
end
